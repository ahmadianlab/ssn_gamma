"""
Usage: python script_parallel_sampler.py <int> --Nparal <int> --NLmodel <0/1>
First positional argument is Nsamp: # of samples
For help run  python script_parallel_sampler.py -h
"""

# ---------------------------------------------------------------
import argparse

parser = argparse.ArgumentParser(description='Sampling SSN and calculating their gamma spectra.')
parser.add_argument('Nsamp', type=int, help='Number of samples')
parser.add_argument('--Nparal', type=int, help='Number of parallel processes', required=False, default=1)
parser.add_argument('--NLmodel', type=int, help='If 1 simulates non-local (NL) model', required=False, default=0)
parser.add_argument('--prefix', type=str, help='A string providing a prefix to filename', required=False, default="")
parser.add_argument('--UOREGON', type=int,
                    help='True if running on the UOregon (ahmadianlab) computer. Only affects save directory',
                    required=False, default=False)

args = parser.parse_args()

Nsamp = args.Nsamp
Nparal = args.Nparal


# ---------------------------------------------------------------
import numpy as np

import time, os  #, json
from datetime import date

import multiprocessing

import io_dict_to_hdf5 as ioh5

import high_levels as hl

attr_dir = lambda A: [d for d in dir(A) if not callable(getattr(A, d)) and d[0] != '_']
turn2dict = lambda A: {att:getattr(A, att) for att in attr_dir(A)}


# ---------------------------------------------------------------
if args.UOREGON: # False # this affects the save_dir only
    save_dir = "/srv/data/data0/yashar/projects_data/Gamma/data" # "/home/yashar/Projects/Gamma/data"
else:
    save_dir = os.path.join(os.getcwd(), "data")

if not os.path.exists(save_dir):
    save_dir = os.getcwd()
    print("\n        !!! Will SAVE to CURRENT DIRECTORY as following path does not exist:\n\t\t" + save_dir)

print("\n          Will save results in " + save_dir + "!\n")

# ---------------------------------------------------------------

contrasts = np.asarray([0, 25, 50, 100])
rad_max = 1. # degrees
radii = np.linspace(0, rad_max, 4+1)[1:] #np.array([0.25 0.5  0.75 1.])

freq_grid_pars = dict(fnums=35,
                      freq_range = np.array([0.1,100]))
corr_time = 5 # changed to 5 on 30/3/2023, used to be 3 before that


t_scale = 1
class ssn_pars():
    n = 2
    k = 0.04
    tauE = 30 # in ms
    tauI = 10 # in ms
    psi = 0.774
    tau_s = np.array([5, 7, 100])*t_scale #in ms, AMPA, GABA, NMDA current decay time constants
    spont_input = np.array([1,1]) * 0 # * 2
    make_J2x2 = lambda Jee, Jei, Jie, Jii: np.array([[Jee, -Jei], [Jie,  -Jii]]) * np.pi * ssn_pars.psi
    make_J2x2_str = "lambda Jee, Jei, Jie, Jii: np.array([[Jee, -Jei], [Jie,  -Jii]]) * np.pi * ssn_pars.psi"

mm_scale = 1 # 5/4
class grid_pars_large():
    gridsize_Nx = 17 # grid-points across each edge
    gridsize_deg = 2 * 1.6 / mm_scale # edge length in degrees
    magnif_factor = 2 * mm_scale # mm/deg
    hyper_col = 800 # mm


J_I_min = 1.1 / (np.pi * ssn_pars.psi)
sampling_ranges = dict(J_min= J_I_min * 2,
                     J_max= J_I_min * 6,
                     J_I_min= J_I_min * 1,
                     J_I_max= J_I_min * 3,
                     g_min= 0.22,
                     g_max= 0.66,
                     NMDA_min= 0.3,
                     NMDA_max= 0.5,
                     p_loc_min=0.25, # 0.30,
                     p_loc_max=0.75, # 0.75,
                     sigXE_min=0.15,
                     sigXE_max=0.60, # was 0.55 before 30/3/2023
                     sigIE_min=0.15,
                     sigIE_max=0.60) # was 0.55 before 30/3/2023
if args.NLmodel:
    sampling_ranges['p_loc_min'], sampling_ranges['p_loc_max'] = 0, 0

other_sampling_pars = dict(BALANCED=True,
                           sigIElarger=True,
                           check_dfdc_2d=True,
                           get_peak_2d='infl_find_peak_freq')


input_pars = dict(contrasts=contrasts,
                  radii=radii,
                  n_probes=5,
                  sigma_Gabor=0.5,
                  sigma_RF=0.04,
                  sig_ori_EF=32,
                  sig_ori_IF=32)

fixed_conn_pars = dict(sigEI=.09,
                       sigII=.09,
                       sigma_oris=45,
                       PERIODIC=False)

class NoisePars(object):
    def __init__(self, stdevE=1.5, stdevI=1.5, corr_time=5, corr_length=0, NMDAratio=0):
        self.stdevE = stdevE
        self.stdevI = stdevI
        self.corr_time = corr_time
        self.corr_length = corr_length
        self.NMDAratio = NMDAratio
noise_pars = NoisePars(corr_time=corr_time)


# ---------------------------------------------------------------
# used only for saving, and not passing to sampler
def make_fixed_pars(ssn, sampler, freq_grid_pars): # has no dependence on globals
    grid_pars_dict = turn2dict(ssn.grid_pars)
    grid_pars_dict.update(cent_EI_inds= ssn.xys2inds([[0,0]]).ravel())

    ssn_pars_dict = turn2dict(sampler.ssn_pars)

    input_pars = sampler.input_pars
    input_pars['spont_input'] = ssn_pars_dict['spont_input']

    noise_pars_dict = turn2dict(sampler.noise_pars)

    conn_pars_fixed = ssn.conn_pars.copy() # will have sigma_oris & PERIODIC (among other things)
    conn_pars_fixed['sigEI'] = conn_pars_fixed['s_2x2'][0,1]
    conn_pars_fixed['sigII'] = conn_pars_fixed['s_2x2'][1,1]
    del conn_pars_fixed['J_2x2'], conn_pars_fixed['s_2x2'], conn_pars_fixed['p_local']

    ssn_psi_pi_fac = sampler.ssn_pars.make_J2x2(*[1,1,1,1])[0,0]
    sampling_ranges = sampler.ranges_dict
    sampling_ranges_paper = sampling_ranges.copy()
    for key in ['J_E_max', 'J_E_min', 'J_I_max', 'J_I_min']:
        sampling_ranges_paper[key] = sampling_ranges_paper[key] * ssn_psi_pi_fac

    return dict(grid_pars_dict=grid_pars_dict,
                ssn_pars_dict=ssn_pars_dict,
                input_pars=input_pars,
                noise_pars_dict=noise_pars_dict,
                conn_pars_fixed=conn_pars_fixed,
                sampling_ranges=sampling_ranges,
                sampling_ranges_paper=sampling_ranges_paper,
                freq_grid_pars=freq_grid_pars)

# ---------------------------------------------------------------
model_name = "fullmodel-" if sampling_ranges['p_loc_max'] > 0 else "NLmodel-"
savefile_prefix = args.prefix + "_" if len(args.prefix) > 0 else args.prefix
fname_base = savefile_prefix + "samples_" + model_name

start_date_str = date.today().strftime("%Y-%m-%d")

fname = fname_base + "-".join([f"Nsamp_{Nsamp}", f"PERIODIC_{int(fixed_conn_pars['PERIODIC'])}",
                           f"magnif_factor_{int(10 * grid_pars_large.magnif_factor):}e-1mm",
                           f"noise_corr_time_{noise_pars.corr_time}ms", start_date_str])
print("\nSave filename: ", fname + '.h5', "\n")

# ---------------------------------------------------------------

def sample(n):
    np.random.seed(0)
    sampler = hl.SamplerRetinoSSN(sampling_ranges_dict=sampling_ranges, **other_sampling_pars,
                                  input_pars=input_pars, fixed_conn_pars=fixed_conn_pars,
                                  ssn_pars=ssn_pars, grid_pars=grid_pars_large, noise_pars=noise_pars)

    return sampler.sample(n, freq_grid_pars=freq_grid_pars, remake_ssn=True), sampler


t0 = time.time()
if Nparal > 1:
    pool = multiprocessing.Pool(processes=Nparal)
    results = [pool.apply_async(sample, args=(Nsamp//Nparal, False)) for proc in range(Nparal)]
    outputs = [res.get() for res in results]
    pool.close()

    # Guide: outputs[i] = (results, j, N_no_2d_cdep, ssn), sampler
    #                      results = dict with keys: params, r_fps, spects, lams, fs, cond_inds
    results = outputs[-1][0][0] # get this one really for its fs & cond_inds which don't depend on sample
    results.update(
        params = np.concatenate([out[0][0]['params'] for out in outputs]),
        r_fps = np.concatenate([out[0][0]['r_fps'] for out in outputs]),
        spects = np.concatenate([out[0][0]['spects'] for out in outputs]),
        lams = np.concatenate([out[0][0]['lams'] for out in outputs]))
    ssn = outputs[-1][0][-1]
    tot_j = sum([out[0][1] for out in outputs])
    N_no_2d_cdep = sum([out[0][2] for out in outputs])
    sampler = outputs[-1][1]

elif Nparal == 1:
    (results, tot_j, N_no_2d_cdep, ssn), sampler = sample(Nsamp)

t = int(time.time() - t0)
simtime_str = f"It took {t//3600}:{(t%3600)//60}:{t%60} hours"
print(simtime_str)

# ---------------------------------------------------------------
save_dict = dict(start_date_str=start_date_str, simtime_str=simtime_str,
                 Nsamp=Nsamp, tot_j=tot_j, N_no_2d_cdep=N_no_2d_cdep)
save_dict.update(results) # results has: params, r_fps, spects, lams, fs, cond_inds
save_dict.update(cent_rs= results['r_fps'][:,:,ssn.xys2inds([[0,0]]).ravel()])

fixed_pars = make_fixed_pars(ssn, sampler, freq_grid_pars)
save_dict.update(fixed_pars=fixed_pars)

# ---------------------------------------------------------------

PERIODIC = fixed_pars['conn_pars_fixed']['PERIODIC']
magnif_factor = fixed_pars['grid_pars_dict']['magnif_factor']
corr_time = fixed_pars['noise_pars_dict']['corr_time']

fname = fname_base + "-".join([f"Nsamp_{Nsamp}", f"PERIODIC_{int(PERIODIC)}",
                           f"magnif_factor_{int(10*magnif_factor):}e-1mm",
                           f"noise_corr_time_{corr_time}ms", start_date_str])

ioh5.save(os.path.join(save_dir, fname + '.h5'), save_dict)

# # to load run:
# data_dic = ioh5.load(os.path.join(save_dir, fname))

print("\n\nSaved to file: ", fname + '.h5', "\n")
