import numpy as np
from util import toeplitz, lin_interp1, welch_spectrum
from ode_solver import solve_ivp
#from jax.ops import index_update


class NoisePars(object):
    def __init__(self, stdevE=1.5, stdevI=1.5, corr_time=5, corr_length=0, NMDAratio=0):
        self.stdevE = stdevE
        self.stdevI = stdevI
        self.corr_time = corr_time
        self.corr_length = corr_length
        self.NMDAratio = NMDAratio

def make_noise_cov(ssn, noise_pars): #, LFPrange):
    # the script assumes independent noise to E and I, and spatially uniform magnitude of noise
    noiseCov = np.hstack( (noise_pars.stdevE**2 * np.ones(ssn.Ne),
                           noise_pars.stdevI**2 * np.ones(ssn.Ni)) )


    if noise_pars.corr_length>0 and ssn.ori_vec.size>1: #assumes one E and one I at every topos
        OriVec = ssn.ori_vec
        dOri = np.abs(OriVec)
        L = OriVec.size * np.diff(OriVec[:2])
        dOri[dOri > L/2] = L-dOri[dOri > L/2] # distance on circle/periodic B.C.
        spatl_filt = toeplitz(np.exp(-(dOri**2)/(2*noise_pars.corr_length**2))/np.sqrt(2*np.pi)/noise_pars.corr_length*L/ssn.Ne)
        sigTau1Sprd1 = 0.394 # roughly the std of spatially and temporally filtered noise when the white seed is randn(ssn.Nthetas,Nt)/sqrt(dt) and corr_time=corr_length = 1 (ms or angle, respectively)
        spatl_filt = spatl_filt * np.sqrt(noise_pars.corr_length/2)/sigTau1Sprd1 # for the sake of output
        spatl_filt = np.kron(np.eye(2), spatl_filt) # 2 for E/I
    else:
        spatl_filt = np.array(1)

    return noiseCov, spatl_filt #, eE #, eI

def linear_power_spect(ssn, rs, noise_pars, freq_range, fnums, LFPrange=None, GammaRange=[20,100], EIGS=False):
    """
    LFPrange = Trgt + [-L:L] = indices contributing to the LFP: voltage
    is averaged over those dimensions and power-spec of that avg is calculated
    noise_pars.stdevE = 1.5; Std of E noise
    noise_pars.stdevI = 1.5; Std of E noise
    noise_pars.corr_time = 5; correlation time of noise in ms
    noise_pars.corr_length = 0.5; correlation length of noise in angles; 0 doesn't work well..: too small response

    example run:
    powspecE = linear_power_spect(ssn, r_fp, NoisePars(), freq_range=[10,100], fnums=50)

    by Yashar Ahmadian -- Nov 2015.
    """
    noiseCov, spatl_filt = make_noise_cov(ssn, noise_pars)
    tau_corr = noise_pars.corr_time /1000 # convert to seconds

    if LFPrange is None:
        LFPrange = [0]
    # setting up e_E and e_I: the projection/measurement vectors for
    # representing the "LFP" measurement (e_E good for LFP interpretation, but e_I ?)
    eE = 1/len(LFPrange) * np.isin(np.arange(ssn.N), LFPrange) # assuming elements of LFPrange are all smaller than ssn.Ne, eE will only have 1's on E elements
    # eI = 1/len(LFPrange) * np.isin(np.arange(ssn.N) - ssn.Ne, LFPrange) # assuming elements of LFPrange are all smaller than ssn.Ne, eE will only have 1's on I elements

    # # above may not work with JAX. If so, use this commented code:
    # eE = np.hstack((
    #     np.array([i in LFPrange for i in range(ssn.Ne)], dtype=np.float32),
    #     np.zeros(ssn.Ni)))

    ones_rcpt = np.ones(ssn.num_rcpt)
    if eE.ndim > 1 and eE.shape[1] > 1:  # case of many different LFP probes (stacked along 2nd axis of eE)
        ones_rcpt = ones_rcpt[:, None]
        noiseCov = noiseCov[:, None]
    eE1 = np.kron(ones_rcpt, eE) # this tensor product by ones(...) is because of the unweighted sum of currents of different types inside the neuronal nonlinearity
    # eI1 = np.kron(ones_rcpt, eI) # this tensor product by ones(...) is because of the unweighted sum of currents of different types inside the neuronal nonlinearity

    #switch-block case 'ampa-gaba-nmda' in MATLAB code
    J = ssn.DCjacobian(rs)
    if EIGS:
        Jacob = ssn.jacobian(J) # np.kron(1/ssn.tau_s, np.ones(ssn.N))[:,None] * J  # equivalent to diag(tau_s) J (math)
        JacobLams = np.linalg.eigvals(Jacob)
    else:
        Jacob = JacobLams = None

    fs = np.linspace(*freq_range,fnums) # grid of frequencies in Hz
    ws = 2*np.pi * fs # angular freq's (omega's) in Hz
    tau_s = np.diag(ssn.tau_s_vec) / 1000 # convert to seconds
    #AnalPowSpecE = np.empty_like(fs)
    AnalPowSpecE = []
    for ii, w in enumerate(ws):
        vecE = np.linalg.solve(
                        (-1j*w * tau_s - J).T.conj() # ssn.inv_G(w,J).T.conj()
                                                  , eE1)
        if ssn.num_rcpt<3: # i.e. if we only have AMPA and GABA (AMPA always first)
            #     vecE = spatl_filt'*reshape(vecE,[ssn.N,ssn.num_rcpt]) # accounting for spatial correlations in noise input
            # !!HERE we ASSUME noise is coming only through the AMPA (m=1) channel... modify LATER for general
            # make sure it's also correct to just ignore the rest of vecE and only take
            # its 1st 1/ssn.num_rcpt (corresponding to AMPA)
            if spatl_filt.size>1:
                vecE = spatl_filt.T  @ vecE[:ssn.N]
            else:
                vecE = spatl_filt * vecE[:ssn.N]
        elif ssn.num_rcpt==3: # i.e. if we have NMDA (which is always the last part)
            # assuming AMPA and NMDA channels (up to scaling) get the exact same realization of noise (i.e. noise cov is rank-deficient)
            if spatl_filt.size>1:
                vecE = spatl_filt.T @ ((1-noise_pars.NMDAratio) * vecE[:ssn.N]  + noise_pars.NMDAratio * vecE[-ssn.N:])
            else:
                vecE = spatl_filt * ((1-noise_pars.NMDAratio) * vecE[:ssn.N]  + noise_pars.NMDAratio * vecE[-ssn.N:])

        noise_spect = 2*tau_corr / np.abs(-1j*w * tau_corr + 1)**2  # power-spec of pink noise (in Hz^{-1}) with time-constant tau_corr and variance 1, which is 2*\tau /abs(-i\omega*\tau + 1)^2 ( FT of its time-domain cov)
        # index_update(AnalPowSpecE, ii,
        #    np.dot(vecE.conj(), noiseCov * vecE) * noise_spect )
        # AnalPowSpecE.append( np.dot(vecE.conj(), noiseCov * vecE) * noise_spect )
        AnalPowSpecE.append( np.sum(vecE.conj() * (noiseCov * vecE), axis=0) * noise_spect )

    # *2 to combine (the symmetric) power across positive and negative freq's:
    AnalPowSpecE = 2 * np.real(np.asarray(AnalPowSpecE)) # shape = (fnums,), or if eE.shape[1]>1, then (fnums, eE.shape[1])

    df = fs[1]-fs[0]
    GammaPower = np.sum(AnalPowSpecE[(GammaRange[0]<fs) & (fs<GammaRange[1])], axis=0) * df # E gamma power

    return AnalPowSpecE, fs, GammaPower, JacobLams, Jacob


    # This was copied here from SSN_classes.py at the end of linear_power_spect
    #
    # # using numba compiled code:
    # Jacob = None if not EIGS else self.jacobian(J) # np.kron(1/ssn.tau_s, np.ones(ssn.N))[:,None] * J  # equivalent to diag(tau_s) J (math)
    # shp = (len(fs),) if e_LFP1.ndim == 1 else (len(fs), e_LFP1.shape[1])
    # return SSN_power_spec.linear_power_spect_loop(N, fs, 0*1j + e_LFP1, shp,
    #                         J, noise_sigsq, spatl_filt, noiseNMDA, tau_s,
    #                         tau_corr, np.asarray(gamma_range), EIGS, 0*1j + Jacob)


#import numba
#@numba.jit(nopython=True, cache=True)
def linear_power_spect_loop(N, fs, e_LFP1, shp, J, noise_sigsq, spatl_filt,
                    noiseNMDA, tau_s, tau_corr, gamma_range, EIGS, Jacob=None):
    """
    perform numba-compiled inner loop of linear_power_spect (see doc of that
    or of the method of the same name defined in SSN_classes.py)
    """
    ws = 2*np.pi * fs # angular freq's (omega's) in Hz

    e_LFP1 = e_LFP1

    # LFP_spectra = []
    LFP_spectra = np.zeros(shp)
    for iw, w in enumerate(ws):
        vecE = np.linalg.solve( (-1j*w * tau_s - J).T.conj() , e_LFP1) # ssn.inv_G(w,J).T.conj() @ e_LFP1

        # ASSUME noise is only coming thru AMPA and NMDA channels (first and last N inds, resp)
        # AND both channels get same exact realization of noise, up to scaling (so noise cov is rank-deficient, with rank ssn.N instead of ssn.dim)
        vecE1 = (1-noiseNMDA) * vecE[:N]  + noiseNMDA * vecE[-N:]
        # account for spatial correlations in noise input
        if spatl_filt.size > 1:
            raise NotImplementedError
            # vecE = spatl_filt.T  @ vecE1
            # vecE1 = vecE
        # power-spec of pink noise with time-constant tau_corr and variance 1, which is 2*\tau /abs(-i\omega*\tau + 1)^2 (FT of exp(-|t|/tau))
        noise_spect = 2* tau_corr/np.abs(-1j*w * tau_corr + 1)**2 # in Hz^{-1}

        # LFP_spectra.append( np.sum(vecE1.conj() * (noise_sigsq * vecE1), axis=0) * noise_spect )
        LFP_spectra[iw] = np.real(np.sum(vecE1.conj() * (noise_sigsq * vecE1), axis=0) * noise_spect)
    # *2 to combine (the symmetric) power across positive and negative freq's:
    # LFP_spectra = 2 * np.real(np.asarray(LFP_spectra))
    LFP_spectra = 2 * LFP_spectra
    # print("shape = ", LFP_spectra.shape)

    # calculate gamma power(s)
    df = fs[1]-fs[0]
    gamma_powers = np.sum(LFP_spectra[(gamma_range[0]<fs) & (fs<gamma_range[1])], axis=0) * df

    # calculate Jacobian and its eigenvalues
    if EIGS:
        JacobLams = np.linalg.eigvals(Jacob)
    else:
        Jacob = JacobLams = None

    return LFP_spectra.T, fs, gamma_powers, JacobLams, Jacob


# ============= simulated power spectrum ==================================================
class SSNSimulator():
    def __init__(self, params, sampler, freq_grid_pars=dict(fnums=35, freq_range=[0.1, 100]),
                linear_spect=True):
        self.sampler = sampler
        self.params = params
        self.freq_grid_pars = freq_grid_pars
        if linear_spect:
            self.linear_PS()
            self.ssn = self.sampler.ssn
        else:
            self.ssn = self.sampler.ssn_retino_PS(params, just_ssn=True) # sampler.ssn

        self.stims = sampler.stims
        self.cent_inds = self.ssn.xys2inds([[0,0]]).ravel()
        self.cond_inds = sampler.cond_inds
        self.contrasts = sampler.input_pars['contrasts']
        self.radii = sampler.input_pars['radii']
        self.inp_dict = dict(cond_inds = sampler.cond_inds,
                             GaborSigma = sampler.input_pars['sigma_Gabor'],
                             contrasts = sampler.input_pars['contrasts'],
                             radii = sampler.input_pars['radii'],
                             dx = self.ssn.grid_pars.dx / self.ssn.grid_pars.magnif_factor)

        self.LFP_inds = self.sampler.e_LFP.nonzero()[0]
        self.LFP_inds_EI = np.hstack((self.LFP_inds, self.LFP_inds + self.ssn.Ne))

        self.fig_num = np.random.randint(10000)

    def linear_PS(self, freq_grid_pars=None, remake_ssn=True):
        freq_grid_pars = self.freq_grid_pars if freq_grid_pars is None else freq_grid_pars
        self.r_fp, self.spect, self.fs, lams, self.CONVG =\
                self.sampler.ssn_retino_PS(self.params, freq_grid_pars=freq_grid_pars, remake_ssn=remake_ssn)
        self.lams = lams * 1000 / (2 * np.pi) # convert to Hz (non-angular freq)

    def simulated_PS(self, ps_func='mine', df=6, Tmin=200, **kwargs):
        """
        df is frequency resolution in Hz
        """
        ts, lfp_t, r_t = self.simulate_ssn(**kwargs)
        dt = ts[1] - ts[0]
        Nmin = int(Tmin / dt)
        x_t = lfp_t[..., Nmin:]
        dt *= 1e-3
        if ps_func == 'mine':
            self.spect_sim, self.fs_sim, Nt_seg = welch_spectrum(x_t, df=df, dt=dt, window_type='hann')
        else:
            import nitime.algorithms as tsa
            self.fs_sim, self.spect_sim, nu = tsa.multi_taper_psd(x_t, Fs=1 / dt, BW=2 * df, adaptive=False)
        self.r_probe_sim = np.mean(r_t[..., Nmin:], axis=-1)

        return ts, lfp_t, r_t

    def simulate_ssn(self, dt=1, Tmax=50000, init_linear=True, LFP_inds=None, noise_std_EI=None, rtol=1e-5):
        """
        Simulates the noise-driven SSN from t=0 to Tmax (in ms)

        Returns
        -------
        t : Time points in ms, shape (Nt,) where Nt = Tmax / dt
        lfp_t : LFP(t) time-series at the time-points. Shape (n_cond-locs, Nt) where
                the first index corresponds to the concatenation of self.cond_inds['c_dep'] and self.cond_inds['gabor_ps']:
                i.e. one probe location (at the center of grid) for each of 4 gratings and 5 for the Gabor stimulus.
        r_t   : firing rate time-series (at the time-points) of E and I at probe locations. Shape = (2, n_cond-locs, Nt)
                First index goes over E and I, while the second index corresponds to the first index of lfp_t.
        """
        Nt = int(np.floor(Tmax / dt))
        Tmax = Nt * dt
        tt = np.arange(0, Nt) * dt

        # make DC input which only goes to AMPA channels
        gE, gI = self.params[4:6]
        inps = np.kron([[1, 0, 0]], np.hstack((gE * self.stims, gI * self.stims))) # (n_conds, ssn.num_rpts * 2 * ssn.Ne)

        ssn, noise_pars = self.ssn, self.sampler.noise_pars
        if noise_std_EI is None:
            noise_std_EI = [noise_pars.stdevE, noise_pars.stdevI]
        else:
            if np.isscalar(noise_std_EI) or len(noise_std_EI) == 1:
                noise_std_EI *= np.ones(2)
        noise_std = np.kron(noise_std_EI, np.ones(ssn.Ne))

        v_dyn = []
        for c, inp in enumerate(inps):
            noise_inp = noise_std[:,None] * self.make_input_noise(dt, Nt)
            noise_inp = np.vstack(((1 - noise_pars.NMDAratio) * noise_inp, noise_pars.NMDAratio * noise_inp, 0 * noise_inp ))
            noise = lambda t: lin_interp1(tt, noise_inp, t, 0, dt)

            dvdt = lambda t, v: ssn.dvdt(v, inp + noise(t))

            if LFP_inds is None:
                if c == self.cond_inds['gabor_fp']:
                    LFP_inds0 = self.LFP_inds_EI
                else:
                    LFP_inds0 = self.cent_inds
            else:
                LFP_inds0 = LFP_inds
            # indices of AMPA/GAMA/NMDA input channels for the (E and I neurons at) LFP locations
            obs_inds = LFP_inds0 + np.arange(ssn.num_rcpt)[:, None] * ssn.N
            if init_linear:
                assert hasattr(self, 'r_fp')
                # use v_a = W_a r_a + I_a   to get initial condition for v
                v_init = ssn.Wrcpt @ self.r_fp[c] + inp
            else:
                v_init = np.zeros(inp.shape)

            sol = solve_ivp(dvdt, (0, Tmax), v_init, t_eval=tt, obs_inds=obs_inds.ravel(), rtol=rtol)
            v_dyn.append(sol.y.reshape((ssn.num_rcpt, 2, -1, Nt)))

        v_dyn = np.concatenate(v_dyn, axis=-2) # (ssn.num_rcpt, 2, n_cond-locs, Nt)
        vtot_EI = np.sum(v_dyn, axis=0)        #               (2, n_cond-locs, Nt)
        r_t = ssn.powlaw(vtot_EI)              #               (2, n_cond-locs, Nt)
        lfp_t = vtot_EI[0]                     #                  (n_cond-locs, Nt)

        return sol.t, lfp_t, r_t


    def make_input_noise(self, dt, Nt):
        N, corr_time = self.ssn.N, self.sampler.noise_pars.corr_time
        Tfilt = 10 * corr_time
        Nfilt = int(Tfilt / dt)
        ttfilt = np.arange(0, Nfilt+1) * dt
        assert Nt > len(ttfilt)
        #the temporal filter.
        filter = np.exp(-ttfilt / corr_time) * dt / corr_time

        white_noise = np.random.randn(N, Nt + Nfilt) / np.sqrt(dt) * np.sqrt(2 * corr_time)

        noise = []
        for nn in range(N):
            colored = np.convolve(white_noise[nn,:], filter, 'valid')
            noise.append(colored)

        return np.array(noise)


# ============= linearized power spectrum by eigen-decomposition ==========================

def eigen_power_spect_1probe(ssn, Jacob, fs, e_LFP, DIAGEIGS=False, mindist_diag=None,
       noise_pars=NoisePars(), eigvals=None, Rvecs=None, Lvecs=None, C_ab=None):
    """
    Returns the power spectrum/a (PS) of "LFP" recorded on a single LFP probe.
    This function does the heavy-lifting for eigen_power_spect.
    See that function's doc.
    """
    # calculate Jacobian and its eigenvalues
    # Jacob = ssn.jacobian(None, r=r_fp)
    if eigvals is None:
        eigvals, Rvecs = np.linalg.eig(Jacob) # Rvecs[:, a] = R_a
        eigvals = eigvals * 1000/2/np.pi # in Hz (and linear freq)
    if Lvecs is None:
        Lvecs = np.linalg.inv(Rvecs) # Lvecs[a, :] = L_a

    # calculate noise spatial covariance
    if C_ab is None:
        noise_sigsq, spatl_filt = ssn.make_noise_cov(noise_pars)
        noiseNMDA = 0 if ssn.num_rcpt<3 else noise_pars.NMDAratio
        assert noiseNMDA == 0 # not implementing for noise in both AMPA and NMDA
        assert spatl_filt.size == 1 # not implementing for noise with spatial correlations
        tau_s = ssn.tau_s_vec # np.diag(ssn.tau_s_vec)
        # assuming spatially uncorrelated noise entering only thru AMPA:
        C_ab = (Lvecs[:,:ssn.N] * noise_sigsq / tau_s[:ssn.N]**2) @ Lvecs.T.conj()[:ssn.N,:] # shape = (dim, dim)

    # calculate probe e's and P_ab
    ones_rcpt = np.ones(ssn.num_rcpt)
    if e_LFP.ndim > 1 and e_LFP.shape[1] > 1:  # case of many different LFP probes (stacked along 2nd axis of e_LFP)
        raise ValueError('e_LFP.ndim must be 1 -- multiple probes not implemented.')
        # ones_rcpt = ones_rcpt[:, None]
        # noise_sigsq = noise_sigsq[:, None]
    e_LFP1 = np.kron(ones_rcpt, e_LFP) # this tensor product by ones(...) is because of the unweighted sum of currents of different types inside the neuronal nonlinearity
    e_a = Rvecs.T.conj() @ e_LFP1 # shape = (dim,)
    P_ab = e_a[:,None] * e_a.conj()[None,:] # shape = (dim, dim)

    A_ab = P_ab.T * C_ab

    if fs is not None:
        # calculate LFP power spectrum/a:
        Gw = 1 / (-1j*fs[None,:] - eigvals[:,None]) * 1000/2/np.pi # shape = (dim, fnums)
        if not DIAGEIGS:
            pre_LFP_spect = np.sum( Gw.conj() * (A_ab.T @ Gw) , axis=0)
        else:
            if mindist_diag is None:
                pre_LFP_spect = np.abs(Gw.T)**2 @ np.diag(A_ab)
            else:
                dists = np.abs(eigvals[:,None] - eigvals[None,:])
                mask = dists <= mindist_diag
                A_ab = A_ab * mask
                pre_LFP_spect = np.sum(Gw.conj() * (A_ab.T @ Gw) , axis=0)

        tau_corr = noise_pars.corr_time /1000 # convert to seconds
        noise_spect = 2*tau_corr/np.abs(-1j*2*np.pi * fs * tau_corr + 1)**2 # has units of Hz^{-1}
        LFP_spect = 2 * pre_LFP_spect * noise_spect # the prefactor of 2 is to add the symmetric power at negative freq's

        return np.real(LFP_spect), eigvals, Rvecs, Lvecs, A_ab, Gw, tau_corr, P_ab, C_ab
    else:
        return eigvals, Rvecs, Lvecs, A_ab, P_ab, C_ab


def diag_spect_eig(fs, Gw, A_ab, tau_corr, mindist_diag=None, eigvals=None):
    """
    An auxiliary function. Returns an approximation to PS in a single probe, in
    which "off-diagonal" contributions in the eigenvalue expansion are dropped.
    """
    if mindist_diag is None:
        pre_spect_diag = np.abs(Gw.T)**2 @ np.diag(A_ab)
    else:
        assert eigvals is not None
        dists = np.abs(eigvals[:, None] - eigvals[None, :])
        mask = dists <= mindist_diag
        A_ab = A_ab * mask
        pre_spect_diag = np.sum(Gw.conj() * (A_ab.T @ Gw), axis=0)

    noise_spect = 2*tau_corr/np.abs(-1j*2*np.pi*fs * tau_corr + 1)**2
    return 2 * np.real( pre_spect_diag * noise_spect)

def eigen_power_spect(ssn, J, fs, e_LFPs, DIAGEIGS=True, mindist_diag=None, noise_pars=NoisePars(),
                      eigvals=None, Rvecs=None, Lvecs=None):
    """
    Returns the power spectrum/a (PS) of "LFP" recorded on 1 or MULTIPLE
    "electrodes" or probes, in the noise-driven multi-synaptic SSN, in a
    SINGLE stimulus condition, by linearizing in noise around the noise-free
    fixed point for that stimulus (the stimulus condition is implicitly specified
    by its fixed point "r_fp"), and using the eigen-decomposition of the
    dynamic Jacobian. When DIAGEIGS is True, an approximation is returned
    where "off-diagonal" contributions in the eigenvalue expansion are dropped.

    LFP is approximated as the total-input into neurons, averaged over a
    group of neurons according to columns of "e_LFP" which provide the
    averaging weights. Different columns of "e_LFP" correspond to different
    probes. Averaging would be accurate if all column-sums of e_LFP are 1.
    Also, since electrophysiologically, LFP corresponds to averaged input
    to Pyramidal cells, it's more biological if e_LFP is only zero
    on inhibitory rows.

    Inputs:
        ssn: the ssn object
        J: dynamic Jacobian at some fixed point
        fs: fnum-element seq of freq's to evaluate PS at
        e_LFPs: shape = (N, n_probes), with each N-dim column being the projection
                or signature vector for a single LFP probe
        DIAGEIGS: if True, it also returns an approximation to PS in which
               "off-diagonal" contributions in the eigenvalue expansion are dropped.
        noise_pars: specifies parameters of noise. Following fields are used
                    (example values are what I had used for the SSNHomogRing model):
                noise_pars.stdevE = 1.5; Std of E noise
                noise_pars.stdevI = 1.5; Std of E noise
                noise_pars.corr_time = 5; correlation time of noise in ms
                noise_pars.corr_length = 0.5; correlation length of noise in angles; 0 doesn't work well..: too small response
                noise_pars.NMDAratio = 0; % of external noise fed to the NMDA channel (the rest goes to AMPA)
    """
    spect_eig, eigs, eigvecs, Leigvecs, A_ab, Gw, tau_corr, P_ab, C_ab = \
                               eigen_power_spect_1probe(ssn, J, fs, e_LFPs[:,0],
                                        eigvals=eigvals, Rvecs=Rvecs, Lvecs=Lvecs)
    if DIAGEIGS: # diagonal approx to eig-decomposition
        spect_diag = diag_spect_eig(fs, Gw, A_ab, tau_corr, mindist_diag, eigs)

    A_abs = [A_ab]
    P_abs = [P_ab]
    for ee in e_LFPs.T[1:]:
        sp = eigen_power_spect_1probe(ssn, J, fs, ee, eigvals=eigs, Rvecs=eigvecs, Lvecs=Leigvecs)    #LFP_spectra =  np.abs(Gw.T)**2 * np.diag(A_ab)
        spect_eig = np.vstack((spect_eig, sp[0]))
        A_abs.append(sp[4])
        P_abs.append(sp[-2])
        if DIAGEIGS:
            spect_diag = np.vstack((spect_diag, diag_spect_eig(fs, Gw, sp[4], tau_corr, mindist_diag, eigs)))
    spect_eig = spect_eig.T #/np.mean(spect_eig)
    if DIAGEIGS:
        spect_diag = spect_diag.T
    else:
        spect_diag = None

    return spect_eig, spect_diag, eigs, eigvecs, Leigvecs, A_abs, Gw, P_abs

def eigen_power_spect_As(ssn, J, e_LFPs, eigvals=None, Rvecs=None, Lvecs=None):
    """
    like eigen_power_spect but only calculates the A_ab, eigenvalues and L/R-eigenvectors of Jacobin.
    Hence doesn't require fs (as it does not calculate (an approximation) to PS)
    """
    eigs, eigvecs, Leigvecs, A_ab, P_ab, _ = eigen_power_spect_1probe(ssn, J, None, e_LFPs[:,0],
                                                                      eigvals=eigvals, Rvecs=Rvecs, Lvecs=Lvecs)
    A_abs = [A_ab]
    P_abs = [P_ab]
    for ee in e_LFPs.T[1:]:
        sp = eigen_power_spect_1probe(ssn, J, None, ee, eigvals=eigs, Rvecs=eigvecs, Lvecs=Leigvecs)
        A_abs.append(sp[3])
        P_abs.append(sp[4])

    return eigs, eigvecs, Leigvecs, A_abs, P_abs




#============================ Finding peak frequencies and widths ======================================

def argmax_peak_freq(fs, spects, normalize_to_spont=True,
                     count_gammapeaks_in_rawPS=True, gamma_rng=[30,90], gamma_rng_wide=[20,120], verbatim=False):
    """
    Computes peak frequencies and half-widths of peaks of power-spectra "spects" in the ENTIRE range defined by "fs".
    "gamma_rng" and "gamma_rng_wide" are only relevant if "count_gammapeaks_in_rawPS" and even then only for checking
    if the unnormalized spectrum has a negative curvature portion (a "peak") in those frequency ranges;
    otherwise peak-freq and half-width are output as NaN.
    Input:
    fs: array of frequencies corresponding to 0-th axis of spects
    spects: powerspectra. spects.shape = (len(fs), # of conds)
    normalize_to_spont: if True it normalizes spects to spects in condition 0, assumed to be spontaneous condition,
    before calculating peaks and widths.
    count_gammapeaks_in_rawPS: if True: it uses  "infl_peak_freqs_1cond" (my function) to see if there are
    any negative curvature peaks in the unnormalized PS, in the range defined by gamma_rng (or really?) gamma_rng_wide.
    """
    if normalize_to_spont:
        spects0 = spects
        spects = spects / (spects[:, 0])[:, None]

    # set peak frequencies to maxima of (possibly normalized) PS
    imax = spects.argmax(axis=0)
    f0 = fs[imax]
    # discard any "peaks" at the start or end of frequency range:
    f0[f0 == fs[-1]] = np.nan
    f0[f0 == fs[0]] = np.nan

    df = fs[1] - fs[0]
    if normalize_to_spont:
        f0[0] = np.nan # set the peak freq of spontaneous case, which post-normalization is constant, to NaN
        hw = np.nan * np.zeros_like(f0)
        # half width at half height/max (HWHM), where height is diff of peak and 1 (= normalized spontanous PS)
        spects = spects - 1 # does this matter?!
        # iceberg = freqs with PS above half height
        iceberg = np.int32(spects > spects.max(axis=0)/2)
        # pad zero to get the same shape after diff
        icediff = np.vstack((np.zeros(iceberg.shape[1]), np.diff(iceberg, axis=0)))
        for c, col in enumerate(icediff.T):
            if c > 0: # conditions other than spontaneous activity, by which we have normalized (so spects[:,0] is zero identically)
                if count_gammapeaks_in_rawPS:
                    n_gamma, *_ = infl_peak_freqs_1cond(fs, spects0[:, c], gamma_rng, gamma_rng_wide)
                else:
                    n_gamma = 1
                if n_gamma == 0:
                    if verbatim:
                        print(f"SSN_power_spec.argmax_peak_freq: n_gamma = 0, at condition {c}")
                    f0[c], hw[c] = np.nan, np.nan
                else:
                    # find HWHM
                    p1_diffs = (col[:imax[c]] == +1).nonzero()[0]
                    last_p1 = 0 if len(p1_diffs) == 0 else p1_diffs[-1] # index of the last pre-peak 1 in iceberg
                    m1_diffs = (col[imax[c]:] == -1).nonzero()[0]
                    first_m1 = len(fs)-1 if len(m1_diffs) == 0 else imax[c] + m1_diffs[0] # index of the first post-peak 0 in iceberg
                    # first_m1 = np.minimum(first_m1, len(fs) - 2) # to prevent out of bounds index
                    fL = fs[last_p1] - df/2 # frequency of midpoint b/w last pre-peak 1 (i.e. in iceberg) and the 0 before it
                    fR = fs[first_m1] - df/2 # frequency of midpoint b/w last post-peak 0 and the 1 (1 = in iceberg) before it
                    nominal_hw = (fR - fL) / 2
                    # nominal_hw sets width of non-interior icebergs to difference with lower or upper fs edges
                    # we correct for this below, requiring that iceberg is not the entire fs range (if so hw = nan)
                    if len(m1_diffs) + len(p1_diffs) > 0:
                        if len(m1_diffs) == 0:
                            hw[c] = np.maximum(f0[c] - fL, nominal_hw)
                        elif len(p1_diffs) == 0:
                            hw[c] = np.maximum(fR - f0[c], nominal_hw)
                        else:
                            hw[c] = nominal_hw
    else:
        # TODO: implement half-width calculation
        raise NotImplemented
        hw = np.nan * np.zeros_like(f0)

    return f0, hw

# def argmax_peak_freq(fs, spects, normalize_to_spont=True):
#     """
#     spects.shape = (len(fs), # of conds)
#     """
#     if normalize_to_spont:
#         spects = spects / (spects[:, 0])[:, None]

#     imax = spects.argmax(axis=0)
#     f0 = fs[imax]
#     if normalize_to_spont:
#         hw = np.nan * np.zeros_like(f0)
#         # half width at half height/max (HWHM), where height is diff of peak and 1 (= normalized spontanous PS)
#         spects = spects - 1
#         iceberg = np.int32(spects > spects.max(axis=0)/2)
#         icediff = np.vstack((np.zeros(iceberg.shape[1]), np.diff(iceberg, axis=0)))
#         for c, col in enumerate(icediff.T[1:]):
#             p1_diffs = (col[:imax[c]] == 1).nonzero()[0]
#             last_p1 = 0 if len(p1_diffs) == 0 else p1_diffs[-1]
#             m1_diffs = (col[imax[c]:] == -1).nonzero()[0]
#             first_m1 = len(fs)-2 if len(m1_diffs) == 0 else imax[c] + m1_diffs[0]
#             first_m1 = np.minimum(first_m1, len(fs) - 2) # to prevent out of bounds index
#             hw[c+1] = (fs[first_m1 + 1] - fs[last_p1]) / 2
#     else:
#         # TODO: implement half-width calculation
#         raise NotImplemented
#         hw = np.nan * np.zeros_like(f0)

#     return f0, hw


def infl_peak_freqs_1cond(fs, spect, gamma_rng, gamma_rng_wide):
    """
    My version of the inflection point method for finding peaks:
                  auxiliary function (core algorithm) for a single condition
    """
    mnts = np.where(np.diff(spect, axis=0, n=2) < 0, 1, 0)  # concave regions (peaks)
    mnts = np.hstack((mnts[0], mnts, mnts[-1])) # previous line finds sign of 2nd derivative in the interior,
           # we assume sign of 2nd-derivative on boundaries match their nearest neighbors'
           # in the interior
    # mnts_inds = np.nonzero(mnts)[0]
    inflctns = np.diff(mnts, axis=0)
    lefts_inds = np.nonzero(inflctns > 0)[0]
    rights_inds = np.nonzero(inflctns < 0)[0]
    df = np.diff(fs[:2])[0]
    lefts_fs = fs[lefts_inds] + df / 2
    rights_fs = fs[rights_inds] + df / 2
    if mnts[0]:
        lefts_fs = np.hstack((fs[0], lefts_fs))
        # lefts_inds = np.hstack((0, lefts_inds))
    if mnts[-1]:
        rights_fs = np.hstack((rights_fs, fs[-1]))
        # rights_inds = np.hstack((rights_inds, len(fs) - 1))
    assert len(rights_fs) == len(lefts_fs)

    n_allpeaks = len(rights_fs)
    f0s = (lefts_fs + rights_fs) / 2
    hws = (rights_fs - lefts_fs) / 2  # matches the half-width for a Lorentzian

    gamminds = (gamma_rng[0] < f0s) & (f0s < gamma_rng[1])
    # if no gamma peak found, look in the broader frequency band gamma_rng_wide:
    if gamminds.sum() == 0:
        gamminds = (gamma_rng_wide[0] < f0s) & (f0s < gamma_rng_wide[1])
    n_gammapeaks = gamminds.sum()
    if n_gammapeaks > 0:
        # return the f0 and hw of the peak with the lowest frequency falling in the gamma band.
        f0_1stgamma, hw_1stgamma = f0s[gamminds][0], hws[gamminds][0]
    else:
        f0_1stgamma, hw_1stgamma = np.nan, np.nan

    return n_gammapeaks, f0_1stgamma, hw_1stgamma, n_allpeaks, f0s, hws


def infl_peak_freqs(fs, spects, gamma_rng=[30,90], gamma_rng_wide=[15,140], normalize_to_spont=True,
                    count_gammapeaks_in_rawPS=True):
    """
    My version of the inflection point method for finding peaks
    spects.shape = (len(fs), # of conditions/f.p.'s)
    normalize_to_spont: if True normalize PS to spontneous PS and then calculate peak freq and half-widths from that.
    count_gammapeaks_in_rawPS: if True and normalize_to_spont also True, count the peaks in the raw (unnormalized) PS
                        and if count is 0, generate NaN for peak freq and half-width (based on normalized PS)
    """

    n_conds = spects.shape[1]
    if normalize_to_spont:
        spects0 = spects
        spects = spects / (spects[:, 0])[:, None]

    # by default, set the f0/hw's to NaN and peak counts to 0
    n_gammapeaks, n_allpeaks = np.zeros(n_conds, dtype=int), np.zeros(n_conds, dtype=int)
    f0_1stgammapeak, hw_1stgammapeak = np.ones(n_conds) * np.nan, np.ones(n_conds) * np.nan
    f0_allpeaks_ls, hw_allpeaks_ls = [np.nan for c in range(n_conds)], [np.nan for c in range(n_conds)]
    for c, spect in enumerate(spects.T): # loop over stimulus conditions
        if not normalize_to_spont or c > 0: # in the normalized case, skip spontaneous cond whose spect is now constant
            n_g, f0_g, hw_g, n_all, f0s, hws = infl_peak_freqs_1cond(fs, spect, gamma_rng, gamma_rng_wide)
            if normalize_to_spont and count_gammapeaks_in_rawPS:
                n_g, *_ = infl_peak_freqs_1cond(fs, spects0[:,c], gamma_rng, gamma_rng_wide)
                if n_g == 0:
                    f0_g, hw_g = np.nan, np.nan
            n_gammapeaks[c], f0_1stgammapeak[c], hw_1stgammapeak[c] = n_g, f0_g, hw_g
            n_allpeaks[c],   f0_allpeaks_ls[c],  hw_allpeaks_ls[c]  = n_all, f0s, hws

    # turn <f0/hw>_allpeaks to numpy arrays
    f0_allpeaks = np.ones((n_conds, n_allpeaks.max())) * np.nan
    hw_allpeaks = np.ones((n_conds, n_allpeaks.max())) * np.nan
    for c, (f0s, hws) in enumerate(zip(f0_allpeaks_ls, hw_allpeaks_ls)):
        f0_allpeaks[c, :n_allpeaks[c]] = f0s
        hw_allpeaks[c, :n_allpeaks[c]] = hws

    # discard any gamma peaks at the start or end of frequency range:
    f0_1stgammapeak[f0_1stgammapeak == fs[-1]] = np.nan
    f0_1stgammapeak[f0_1stgammapeak == fs[0]] = np.nan


    return f0_1stgammapeak, hw_1stgammapeak, n_gammapeaks, f0_allpeaks, hw_allpeaks, n_allpeaks





#============================ by Caleb ======================================

#def find_peak_freq(fs, spect, start_ind=4):
def find_peak_freq(fs, spect, f_min=10): #Yashar modified this, using f_min
    '''
    Find's the peak frequency a la Ray and Maunsell. Subtracts the background spect (BS)
    when contrast = 0, from each spect and finds peak above a minimum frequency (f_min)
    '''
    spect = np.asarray(spect)
    if np.mean(np.real(spect)) != 1:
        spect = np.real(spect)/np.mean(np.real(spect))

    BS = spect[:, 0] #spectrum with no stimulus present; BS = Background Spectrum

    d_spect = spect[:, 1:] - BS[:, None] #difference between stimulus present and background spectrum

    # find first index in fs above f_min
    start_ind = (fs > f_min).nonzero()[0][0]
    return fs[np.argmax(d_spect[start_ind:, :], axis=0) + start_ind]

    # # find the location of the peak above f_min
    # fs1 = fs[fs > f_min]
    # return fs1[np.argmax(d_spect[fs > f_min, :], axis=0)]

def infl_find_peak_freq(fs, spect, normalize_to_spont=False):
    '''
    Caleb's version.
    function that finds the peak frequency using the inflectin point method.
    The peak frequency (f0) is assumed to be in the middle of two inflection pts.
    The half width is the distance in frequency between the two inflection points/2.

    inputs
    fs = array of frequency
    spect = array of power spectra, dims are usually freq x contrasts(+probes)

    returns
    f0_out = greatest frequency with peak
    '''

    if normalize_to_spont:
        spect = spect / (spect[:, 0])[:, None]

    nps = 12  # max number of inflection pts
    nmaxpeaks = int(np.floor(nps / 2))
    cons = spect.shape[1]

    spect = np.real(spect[:, :])
    Dspect = np.diff(spect, axis=0, n=2)

    pos_curvature = np.where(Dspect > 0, 1, 0)
    Inf_inds = np.where(np.diff(pos_curvature, axis=0) != 0, 1, 0)

    cc, ff = np.nonzero(Inf_inds.T)
    # cc is a 1D array of contrast indices
    # ff is a 1D array of freq indices

    ff += 1  # because diff(spect) loses an index, i.e. if spect has length 30, dspect has length 28, so shift the freq indices up

    f0 = np.empty((cons, nmaxpeaks))
    hw = np.empty((cons, nmaxpeaks))
    # err = 1 means that power spectrum was too inflecty, i.e. jagged
    err = np.zeros((cons))
    infpt1 = np.empty((cons))
    infpt2 = np.empty((cons))

    f0[:] = np.nan
    hw[:] = np.nan
    infpt1[:] = np.nan
    infpt2[:] = np.nan

    # jj indexes number of max peaks
    jj = 0

    # bunch of if statements to find the peak frequencies, f0, and  half width, hw
    for c in np.arange(len(cc)):

        # is there a peak before the end of frequencies
        if np.all(pos_curvature[ff[c]:, cc[c]] == 0):
            if cc[c] == 0:
                # report an error if I get a gamma bump at 0 contrast
                err[cc[c]] = 1

            # find f0 and hw when it doens't re-bend before fs = max(fs)
            # there still could be a bump, but not another inflection point cause I ran out of freqs
            if jj < nmaxpeaks:
                start_ind = ff[c]
                f0[cc[c], jj] = (fs[-1] + fs[start_ind]) / 2
                hw[cc[c], jj] = (fs[-1] - fs[start_ind]) / 2
            else:
                err[cc[c]] = 1
            # gotta reset jj
            jj = 0

        elif c == len(cc) - 1:
            # if this is the last c, and it's negatively curved, just reset jj.
            jj = 0

        elif np.abs(cc[c + 1] - cc[c]) < 0.1:
            if cc[c] == 0:
                # report an error if I get a gamma bump at 0 contrast
                err[cc[c]] = 1.4

            # find f0 and hw of gamma peak that re-bends before fs = max(fs)
            # if statement makes sure the contrasts doesn't change
            end_ind = ff[c + 1]
            start_ind = ff[c]

            # make sure everything is negatively curved, i.e. not positively curved
            if np.all(pos_curvature[start_ind:end_ind - 1, cc[c]] == 0):
                if jj < nmaxpeaks:
                    f0[cc[c], jj] = (fs[end_ind] + fs[start_ind]) / 2
                    hw[cc[c], jj] = (fs[end_ind] - fs[start_ind]) / 2

                    jj += 1
                    infpt1[cc[c]] = fs[start_ind]
                    infpt2[cc[c]] = fs[end_ind]

                else:
                    err[cc[c]] = 1
            else:
                err[cc[c]] = 2
        else:
            # catch all other cases
            jj = 0

    f0_out = np.empty(cons)
    hw_out = np.empty(cons)

    for c in np.arange(cons):
        if np.all(np.isnan(f0[c])):
            f0_out[c] = np.nan
            hw_out[c] = np.nan
        else:
            f0_out[c] = np.nanmax(f0[c])
            peak_ind = np.nanargmax(f0[c])
            hw_out[c] = hw[c][peak_ind]


    return f0_out, hw_out, err, infpt1, infpt2


#  ------------- my unfinished modified version of caleb's infl_find_peak_freq function --------------------
#
#     nps = 12 # max number of inflection pts
#     nmaxpeaks = int(np.floor(nps/2))
#     cons = spect.shape[1]
#
#     D2spect = np.diff(np.real(spect), axis=0, n=2) # 2nd derivative (diff)
#     pos_curvature = np.where(D2spect > 0, 1, 0)
#
#     # ff and cc are two 1D arrays (of same length) containing, resp., the freq & stim-cond indices of inflection pts.
#     ff, cc = np.nonzero(np.diff(pos_curvature, axis=0))
#     ff += 1 #because np.diff loses an index shift up to indices in Dspect
#
#     f0 = np.empty((cons, nmaxpeaks))
#     hw = np.empty((cons, nmaxpeaks))
#     #err = 1 means that power spectrum was too inflecty, i.e. jagged
#     err = np.zeros((cons))
#     infpt1 = np.empty((cons))
#     infpt2 = np.empty((cons))
#
#     f0[:] = np.nan
#     hw[:] = np.nan
#     infpt1[:] = np.nan
#     infpt2[:] = np.nan
#
#
#     jj = 0 #jj indexes max peaks
#     # loop over inflection points found (including inflection points in all stimulus conditions)
#     for c in np.arange(len(cc)):
#
#         # is this the last inflection point and also one which ends a peak?
#         if np.all(pos_curvature[ff[c]:, cc[c]] == 0):
#             if cc[c] == 0:
#                 # report an error if I get a gamma bump at 0 contrast
#                 err[cc[c]] = 1
#
#             #find f0 and hw when it doens't re-bend before fs = max(fs)
#             #there still could be a bump, but not another inflection point cause I ran out of freqs
#             if jj < nmaxpeaks:
#                 start_ind = ff[c]
#                 f0[cc[c], jj] = (fs[-1] + fs[start_ind])/2
#                 hw[cc[c], jj] = (fs[-1] - fs[start_ind])/2
#             else:
#                 err[cc[c]] = 1
#             #gotta reset jj
#             jj = 0
#
#         # is this the
#         elif c == len(cc)-1:
#             # if this is the last c, and it's negatively curved, just reset jj.
#             jj = 0
#
#         elif np.abs(cc[c+1] - cc[c]) <0.1:
#             if cc[c] == 0:
#                 # report an error if I get a gamma bump at 0 contrast
#                 err[cc[c]] = 1.4
#
#             #find f0 and hw of gamma peak that re-bends before fs = max(fs)
#             #if statement makes sure the contrasts doesn't change
#             end_ind = ff[c+1]
#             start_ind = ff[c]
#
#             #make sure everything is negatively curved, i.e. not positively curved
#             if np.all(pos_curvature[start_ind:end_ind-1, cc[c]] == 0):
#                 if jj < nmaxpeaks:
#                     f0[cc[c], jj] = (fs[end_ind] + fs[start_ind])/2
#                     hw[cc[c], jj] = (fs[end_ind] - fs[start_ind])/2
#
#                     jj+=1
#                     infpt1[cc[c]] = fs[start_ind]
#                     infpt2[cc[c]] = fs[end_ind]
#
#                 else:
#                     err[cc[c]] = 1
#             else:
#                 err[cc[c]]=2
#         else:
#             #catch all other cases
#             jj = 0
#
#     f0_out = np.empty(cons)
#     hw_out = np.empty(cons)
#
#     for c in np.arange(cons):
#         if np.all(np.isnan(f0[c])):
#             f0_out[c] = np.nan
#             hw_out[c] = np.nan
#         else:
#             f0_out[c] = np.nanmax(f0[c])
#             peak_ind = np.nanargmax(f0[c])
#             hw_out[c] = hw[c][peak_ind]
#
#     return f0_out, hw_out, err, infpt1, infpt2

