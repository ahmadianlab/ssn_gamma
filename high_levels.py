from tqdm import tqdm

import numpy as np
#import jax.numpy as np

from dataclasses import dataclass

import SSN_classes
import SSN_power_spec # as SSN_power_spec

# ------------------------------------------------------------------------

#fixed point algorithm:
dt = 1
xtol = 1e-5
Tmax = 5000

# Default power spectrum resolution and range
freq_grid_pars = dict(fnums=35,
                      freq_range = [0.1,100])
# freq_grid_pars = dict(fnums=30,
#                       freq_range = [15,100])

get_peak_dict = dict(infl_find_peak_freq=SSN_power_spec.infl_find_peak_freq,
                     argmax_peak_freq=SSN_power_spec.argmax_peak_freq,
                     infl_peak_freqs=SSN_power_spec.infl_peak_freqs)


@dataclass
class GridPars:
    gridsize_Nx: int # grid-points across each edge
    gridsize_deg: float # edge length in degrees
    magnif_factor: float # mm/deg
    hyper_col: float # mm
    cent_EI_inds: np.array = None

@dataclass
class SSNPars:
    n: float
    k: float
    tauE: float # in ms
    tauI: float # in ms
    psi: float
    tau_s: np.ndarray #in ms, AMPA, GABA, NMDA current decay time constants
    spont_input: np.ndarray # THE DEFAULT in fig4_from_params is 2
    # def make_J2x2(self, Jee, Jei, Jie, Jii):
    #     return np.array([[Jee, -Jei], [Jie,  -Jii]]) * np.pi * psi # * SSNPars.psi

@dataclass
class NoisePars():
    stdevE: float
    stdevI: float
    corr_time: float
    corr_length: float
    NMDAratio: float


class ssn_pars():
    pass

class ssn_pars_2dV1():
    pass

class grid_pars_small():
    pass

class noise_pars():
    pass




# class NoisePars(object):
#     def __init__(self, stdevE=1.5, stdevI=1.5, corr_time=5, corr_length=0, NMDAratio=0):
#         self.stdevE = stdevE
#         self.stdevI = stdevI
#         self.corr_time = corr_time
#         self.corr_length = corr_length
#         self.NMDAratio = NMDAratio
#
# # 2D SSN parameters
# t_scale = 1
# class ssn_pars1():
#     n = 2
#     k = 0.04
#     tauE = 20 # in ms
#     tauI = 10 # in ms
#     psi = 0.774
#     tau_s = np.array([4, 5, 100])*t_scale #in ms, AMPA, GABA, NMDA current decay time constants
#     spont_input = np.array([1,1]) * 2 # * 0
#     make_J2x2 = lambda Jee, Jei, Jie, Jii: np.array([[Jee, -Jei], [Jie,  -Jii]]) * np.pi * ssn_pars1.psi
#
# class ssn_pars2():
#     n = 2.2
#     k = 5 # Hz
#     tauE = 20 # in ms
#     tauI = 10 # in ms
#     psi = 10
#     tau_s = np.array([4, 5, 100])*t_scale #in ms, AMPA, GABA, NMDA current decay time constants
#     spont_input = np.array([1,1]) * 1 # * 0
#     make_J2x2 = lambda Jee, Jei, Jie, Jii: np.array([[Jee, -Jei], [Jie,  -Jii]]) * ssn_pars2.psi
#
# # ------------------------------------------------------------------------
#
# # Retino SSN parameters
# t_scale_2dV1 = 1
# class ssn_pars_2dV1():
#     n = 2
#     k = 0.04
#     tauE = 20 # in ms
#     tauI = 5 # in ms
#     psi = 0.774
#     tau_s = np.array([5, 7, 100])*t_scale_2dV1 #in ms, AMPA, GABA, NMDA current decay time constants
#     spont_input = np.array([1,1]) * 1 # * 0
#     make_J2x2 = lambda Jee, Jei, Jie, Jii: np.array([[Jee, -Jei], [Jie,  -Jii]]) * np.pi * ssn_pars.psi
#
# class grid_pars_small():
#     gridsize_Nx = 11 # grid-points across each edge
#     gridsize_deg = 2 # edge length in degrees
#     magnif_factor = 2 # mm/deg
#     hyper_col = 80 # mm
#
# class grid_pars_large():
#     gridsize_Nx = 16 # grid-points across each edge
#     gridsize_deg = 3 # edge length in degrees
#     magnif_factor = 2 # mm/deg
#     hyper_col = 80 # mm


# ==============================================================================

rand_samp = lambda pmin, pmax: pmin + (pmax - pmin) * np.random.rand()


class Sampler2DSSN():
    """
    sampler object for 2-neuron SSN's: samples random parameter SSN and gets their f.p.'s and spectra at different
    contrasts.
    """
    def __init__(self, sampling_ranges_dict,  contrasts, BALANCED=True,
                 ssn_pars=ssn_pars, noise_pars=noise_pars):
        self.param_names = ['Jee', 'Jei', 'Jie', 'Jii', 'ge', 'gi', 'ρN']
        if sampling_ranges_dict is not None: # secondary use of class is just to use retino_SSN_fixedparams
            self.make_par_ranges_array(sampling_ranges_dict)
        self.contrasts = contrasts
        self.sampling_hyper = dict(BALANCED=BALANCED)
        self.ssn_pars = ssn_pars
        self.ssn.noise_pars = noise_pars
        self.spont_input = ssn_pars.spont_input

    def make_par_ranges_array(self, ranges_dict):
        self.ranges_dict = ranges_dict
        keys = ranges_dict.keys()

        new_keys = ['J_E_min', 'J_I_min', 'J_E_max', 'J_I_max']
        old_keys = ['J_min',   'J_min',   'J_max',   'J_max']
        for new, old in zip(new_keys, old_keys):
            self.ranges_dict[new] = ranges_dict[new] if new in keys else ranges_dict[old]
        del ranges_dict['J_min'], ranges_dict['J_max']

        range_keys = ['J_E', 'J_I', 'J_E', 'J_I', 'g', 'g', 'NMDA']
        self.par_ranges = np.zeros((len(self.param_names), 2))
        for i, key in enumerate(range_keys):
            self.par_ranges[i] = [self.ranges_dict[key + '_min'], self.ranges_dict[key + '_max']]


    def ssn_2D_PS(self, params, spect_conds=None, r_fps=None, freq_grid_pars=freq_grid_pars):
        '''
        finds the fixed points and LFP spectra of the "ssn" object with modified parameters
        encoded in "params" for all stimulus inputs encoded in "stims".

        NOTE: seems like the grid_pars input argument is redundant/unused
        r_fps: if not None, it must have shape = (stims.shape[0], ssn.N), or really (b/c in this case stims is not used)
               it must just have shape = (anything>0, ssn.N)
        OUT:

        '''
        contrasts, ssn_pars, noise_pars = self.contrasts, self.ssn_pars, self.noise_pars

        ssn = SSN_classes.SSN_2D_AMPAGABA(ssn_pars.n, ssn_pars.k, ssn_pars.tauE, ssn_pars.tauI,
                                               *params, tau_s=ssn_pars.tau_s, NMDAratio=params[6])
        ssn.NMDAratio = params[6]
        gE, gI = params[4:6]

        spect = []
        lams = []
        rs = []
        fs = None
        r_init = np.zeros(ssn.N)
        for con in contrasts:
            inp_vec = np.array([gE,gI]) * con + self.spont_input
            r_fp, CONVG = ssn.fixed_point_r(inp_vec, r_init=r_init, Tmax=Tmax, dt=dt, xtol=xtol)
            r_init = r_fp
            
            if not CONVG:
                break
                
            psE, fs, _, jac_eigs, _ = ssn.linear_power_spect(r_fp, noise_pars, **freq_grid_pars, EIGS=True)

            rs.append(r_fp)
            spect.append(psE)
            lams.append(jac_eigs)

        rs = np.array(rs)    # shape = (#contrasts, 2)
        spect = np.array(spect) # shape = (#contrasts, #freqs)
        lams = np.array(lams)   # shape = (#contrasts, 6)

        return rs, spect, fs, lams, ssn


    def sample_params(self):
        params = []
        for range in self.par_ranges:
            params.append(rand_samp(*range))

        return params


    def sample(self, Nsamps, freq_grid_pars=freq_grid_pars):
        """
        :signature: sample(Nsamps, params_2d=None, freq_grid_pars=freq_grid_pars)
        :param Nsamps: number of samples
        :return: arrays of sampled
                params, rs, spects, eigenvalues, j, fs, cond_inds, ssn
        """
        get_peak_2d = self.get_peak_2d # if get_peak_2d is None else get_peak_2d
        BALANCED = self.sampling_hyper['BALANCED'] # if  BALANCED is None else BALANCED
        check_dfdc_2d = self.sampling_hyper['check_dfdc_2d'] # if  check_dfdc_2d is None else check_dfdc_2d

        smp = 0
        params_list = []
        rs_list = []
        spect_list = []
        lams_list = []
        fs = None # just in case we don't get any samples
        pbar = tqdm(total=Nsamps, desc="% Samples")
        for j in range(100*Nsamps):
            params = self.sample_params()

            Jee, Jei, Jie, Jii = params[:4]
            ge, gi = params[4:6]
            # guarantee positive det(J2x2):
            if not (Jee/Jie < Jei/Jii):
                continue
            # if BALANCED=True, guarantee positive Omega's
            if BALANCED and not (Jei/Jii < ge/gi):
                continue

            rs, spect, fs, lams, _ = self.ssn_2D_PS(params, freq_grid_pars=freq_grid_pars)
            # check if all contrasts had converged to f.p.
            if len(rs) < len(self.contrasts):
                continue

            smp += 1
            pbar.update(1)
            pbar.set_postfix({"samp per j ": int(100 * (smp + 1)/ (j + 1)), "samp ": smp})

            params_list.append(params)
            rs_list.append(rs)
            spect_list.append(spect)
            lams_list.append(lams)

            if smp == Nsamps:
                break

        # set self.ssn for playing outside
        ssn_pars = self.ssn_pars
        self.ssn = SSN_classes.SSN_2D_AMPAGABA(ssn_pars.n, ssn_pars.k, ssn_pars.tauE, ssn_pars.tauI,
                                               *params, tau_s=ssn_pars.tau_s, NMDAratio=params[6])

        results = dict(params=np.asarray(params_list), r_fps=np.asarray(rs_list),
                       spects=np.asarray(spect_list), lams=np.asarray(lams_list), fs=fs)

        return results, j, self.ssn


# ===========================================================================

class SamplerRingSSN():
    """
    sample Ring SSN's and get their f.p.'s and spectra for stimuli of different strengths (contrasts)
    """
    def __init__(self, sampling_ranges_dict, input_pars, fixed_conn_pars,
                 BALANCED=True, sigIElarger=False, check_dfdc_2d=True, get_peak_2d='infl_find_peak_freq',
                 ssn_pars=ssn_pars_2dV1, noise_pars=noise_pars):
        """
        sample Ring SSN's and get their f.p.'s and spectra for stimuli of different strengths (contrasts)

            self.input_pars = dict(contrasts=contrasts)

            self.fixed_conn_pars = dict(Ne=50,
                                        L=180,
                                        dist="cos",
                                        L1normalize=False)

        """
        self.param_names = ['Jee', 'Jei', 'Jie', 'Jii', 'ge', 'gi', 'ρN', 'wEE', 'wIE', 'wEI', 'wII', 'wEF', 'wIF']
        if sampling_ranges_dict is not None: # secondary use of class is just to use retino_SSN_fixedparams
            self.make_par_ranges_array(sampling_ranges_dict)
        self.sampling_hyper = dict(BALANCED=BALANCED, sigIElarger=sigIElarger, check_dfdc_2d=check_dfdc_2d)
        self.ssn_pars = ssn_pars
        self.input_pars = input_pars
        self.fixed_conn_pars = fixed_conn_pars
        self.get_peak_2d = get_peak_dict[get_peak_2d]
        self.spont_input = ssn_pars.spont_input


        self.ssn = SSN_classes.SSNHomogRing_AMPAGABA(ssn_pars.n, ssn_pars.k, ssn_pars.tauE, ssn_pars.tauI,
                                                     tau_s=ssn_pars.tau_s, **fixed_conn_pars)

        self.ssn.noise_pars = noise_pars # for historical reasons
        self.noise_pars = noise_pars

        out = self.make_RayMaunsell2010_inputs(ONLY_E=True, **self.input_pars)
        self.stims, self.cond_c_r, c_dep_inds, r_dep_inds, _, input_pars = out
        self.input_pars.update(input_pars)

        # probes go from 0 to 1 degrees of v.a. away from Gabor center along x-axis
        # last_prb = np.maximum(1, grid_pars_large.gridsize_deg/2) * ssn.grid_pars.magnif_factor
        # probe_xys = np.vstack((np.linspace(0,last_prb,n_probes), np.zeros(n_probes))).T
        self.probe_xys = np.vstack((np.arange(self.n_probes) * self.ssn.grid_pars.dx, np.zeros(self.n_probes))).T
        self.e_LFP = self.ssn.make_eLFP_from_xy(self.probe_xys, LFPradius=0.1, unit_xys="mm")

        gabor_ps_inds = self.stims.shape[0] - 1 + np.arange(self.e_LFP.shape[1])
        self.cond_inds = dict(c_dep=c_dep_inds, r_dep=r_dep_inds,
                              gabor_fp=self.stims.shape[0] - 1, gabor_ps=gabor_ps_inds)


    def make_par_ranges_array(self, ranges_dict):
        self.ranges_dict = ranges_dict
        keys = ranges_dict.keys()

        new_keys = ['J_E_min', 'J_I_min', 'sigEE_min', 'sigIE_min']
        old_keys = ['J_min',   'J_min',   'sigXE_min', 'sigXE_min']
        for new_min, old_min in zip(new_keys, old_keys):
            self.ranges_dict[new_min] = ranges_dict[new_min] if new_min in keys else ranges_dict[old_min]
            new_max, old_max = new_min.replace('min', 'max'), old_min.replace('min', 'max')
            self.ranges_dict[new_max] = ranges_dict[new_max] if new_max in keys else ranges_dict[old_max]
        del ranges_dict['J_min'], ranges_dict['J_max'], ranges_dict['sigXE_min'], ranges_dict['sigXE_max']

        range_keys = ['J_E', 'J_I', 'J_E', 'J_I', 'g', 'g', 'NMDA', 'p_loc', 'p_loc', 'sigEE', 'sigIE']
        self.par_ranges = np.zeros((len(self.param_names), 2))
        for i, key in enumerate(range_keys):
            self.par_ranges[i] = [self.ranges_dict[key + '_min'], self.ranges_dict[key + '_max']]


    def make_RayMaunsell2010_inputs(self, contrasts, radii, sigma_Gabor, sigma_RF, n_probes=None,
                                           sig_ori_EF=None, sig_ori_IF=None, spont_input=None, ONLY_E=False):
        """
        make full SSN input vectors for all the conditions of Ray & Maunsell as well
        as condition necessary for getting a size tuning curve.
        The conditions will be, in order, gratings of all "contrasts" at maximum
        radius (i.e. max(radii)), skipping the max(contrasts) & max(radii) grating (to avoid repetition),
        then gratings of all "radii" at max contrasts (i.e. max(contrasts)), and then
        the Ray & Maunsell 2010's Gabor stimulus.

        In:
            ssn: object of type SSN_classes.SSN2DTopoV1 or its descendents
            contrasts: vector of different contrasts (assumed to be in increasing order)
            radii: vector of different radii (assumed to be in increasing order)
            sigma_Gabor: (scalar) the sigma of the Gabor stimulus
            sigma_RF: the margin size (= V1 receptive field sigma) for grating stimulus inputs.
            ONLY_E: if True only output the E components of the stimuli
        Out:
            stims: shape = (#conditions, #neurons) condition (where #neuron = ssn.N)
                            and #conditions = len(contrasts) + len(radii)
            cond_c_r: shape = (#conditions, 2), cond_c_r[0] gives the (peak) contrast
                      in different conditions, while cond_c_r[0] gives the radius of sigma_Gabor
            c_dep_inds: indices of conditions relevant for contrast tuning/dependence
            r_dep_inds: indices of conditions relevant for size tuning/dependence
            gabor_inds: index of the Gabor stimulsu condition
        """
        assert isinstance(self.ssn, SSN_classes.SSN2DTopoV1)
        ssn = self.ssn

        conts = np.hstack((contrasts, np.ones(len(radii))*np.max(contrasts)))
        rads = np.hstack((np.max(radii)*np.ones(len(contrasts)-1), radii))
        cond_c_r = np.vstack((conts, np.hstack((rads, sigma_Gabor)))).T

        c_dep_inds = np.r_[0:len(contrasts)-1, len(contrasts)+len(radii)-2] # indices of contrast-dependence conditions
        r_dep_inds = np.r_[len(contrasts)-1:len(contrasts)+len(radii)-1] # indices of contrast-dependence conditions
        gabor_inds = np.array(len(cond_c_r)-1)

        input_pars = dict(contrasts=contrasts, radii=radii,
                          sigma_RF=sigma_RF, sigma_Gabor=sigma_Gabor)
        kwargs = {}
        if sig_ori_EF is not None:
            kwargs['sig_ori_EF'] = sig_ori_EF
            kwargs['sig_ori_IF'] = sig_ori_IF if sig_ori_IF is not None else sig_ori_EF
        input_pars.update(kwargs)


        stims = []
        for c, r in cond_c_r[:-1]:
            stims.append( c * ssn.make_grating_input(r, sigma_RF=sigma_RF, ONLY_E=ONLY_E, **kwargs) )
        stims.append( 100 * ssn.make_gabor_input(sigma_Gabor, ONLY_E=ONLY_E, **kwargs))

        return np.vstack(stims), cond_c_r, c_dep_inds, r_dep_inds, gabor_inds, input_pars


    # ssn_retino_FP finds the PS and fixed point
    def ssn_retino_PS(self, params, freq_grid_pars=freq_grid_pars, r_fps=None, spect_conds=None, remake_ssn=False,
                      verbatim=False, just_ssn=False):
        '''
        finds the fixed points and LFP spectra of the "ssn" object with modified parameters
        encoded in "params" for all stimulus inputs encoded in "stims".

        NOTE: seems like the grid_pars input argument is redundant/unused
        r_fps: if not None, it must have shape = (stims.shape[0], ssn.N), or really (b/c in this case stims is not used)
               it must just have shape = (anything>0, ssn.N)
        OUT:

        '''
        remake_ssn = True if just_ssn else remake_ssn
        if remake_ssn:
            self.ssn = SSN_classes.SSN2DTopoV1_AMPAGABA(self.ssn.n, self.ssn.k, self.ssn.neuron_params['tauE'],
                self.ssn.neuron_params['tauI'], self.grid_pars, conn_pars=None, ori_map=self.ssn.ori_map, tau_s=self.ssn.tau_s)
            self.ssn.noise_pars = self.noise_pars # for historical reasons

        ssn, stims, e_LFP = self.ssn, self.stims, self.e_LFP
        ssn_pars, grid_pars, noise_pars = self.ssn_pars, self.grid_pars, self.noise_pars

        #unpack parameters
        gE, gI, NMDAratio, p_local_EE = params[4:8]
        p_local_IE = params[8] if (len(params) == 11) else p_local_EE
        sigEE, sigIE = params[9:11] if (len(params) == 11) else params[8:10]

        conn_pars = dict(
            J_2x2 = ssn_pars.make_J2x2(*params[:4]),
            s_2x2 = np.array([[sigEE, self.fixed_conn_pars['sigEI']],
                              [sigIE, self.fixed_conn_pars['sigII']]]), # in mm
            p_local = [p_local_EE, p_local_IE],
            sigma_oris=self.fixed_conn_pars['sigma_oris'],
            PERIODIC=self.fixed_conn_pars['PERIODIC'])

        ssn.make_W(**conn_pars)
        ssn.NMDAratio = NMDAratio

        if just_ssn: # just return the ssn with made W with given parameters
            return ssn
        else: # calculate fixed point r's, PS's, and Jacobian eigenvalues
            if r_fps is None:
                inps = np.hstack((gE * stims, gI * stims)).T # shape: (ssn.N, #conditions)
                # for inp_vec in inps.T:
                #     r_init = np.zeros(ssn.N)
                #     r_fp, CONVG = ssn.fixed_point_r(inp_vec, r_init=r_init, Tmax=Tmax, dt=dt, xtol=xtol)
                r_init = np.zeros(inps.shape)
                r_fps, CONVG = ssn.fixed_point_r(inps, r_init=r_init, Tmax=Tmax, dt=dt, xtol=xtol)
                r_fps = r_fps.T # shape = (#conditions, ssn.N) = (stims.shape[0], ssn.N)
            else:
                assert r_fps.shape[0] > 0 and r_fps.shape[1] == ssn.N

            spect = []
            lams = []
            fs = None
            if CONVG:
                if verbatim: # for debugging
                    attr_dir1 = lambda A, sep=" | ": print(sep.join(sorted([f"{d}: {(A.__dict__)[d]}" for d in dir(A) if not callable(getattr(A, d)) and d[:1] != '_'])))
                    print("spect_conds = ", spect_conds, "\ne_LFP nonzero: ", e_LFP.nonzero()[0], "\nfreq_grid_pars", freq_grid_pars, "\nnoise_pars", attr_dir1(noise_pars))

                # first get LFP-PS at center probe in all (if spect_conds is None) grating conditions:
                r_fps_spect = r_fps[:-1] if spect_conds is None else r_fps[spect_conds]
                for r_fp in r_fps_spect:
                    psE, _, _, jac_eigs, _ = ssn.linear_power_spect(r_fp, noise_pars,
                                             **freq_grid_pars, e_LFP=e_LFP[:,0], EIGS=True)
                    spect.append(psE)
                    lams.append(jac_eigs)
                spect = np.asarray(spect)

                # then get LFP-PS at all probes in Gabor condition
                psE, fs, _, jac_eigs, _ = ssn.linear_power_spect(r_fps[-1], noise_pars,
                                                  **freq_grid_pars, e_LFP=e_LFP, EIGS=True)
                spect = np.vstack((spect, psE)) # shape: (stims.shape[0] + e_LFP.shape[1]-1, # freqs) if spect_conds is None
                lams.append(jac_eigs)
                lams = np.asarray(lams) # shape = (stims.shape[0], ssn.dim) if spect_conds is None

                if verbatim: # for debugging
                    print(".ssn_retino_PS: SD(spect, axis=1)[2:5]", np.std(spect, axis=1)[2:5])
                    print(".ssn_retino_PS: SD(r_fps, axis=1)[2:5]", np.std(r_fps, axis=1)[2:5])
                    print(".ssn_retino_PS: params = ", params)

            return r_fps, spect, fs, lams, CONVG


    def ssn_2D_PS(self, params, contrasts, spect_conds=None, r_fps=None, freq_grid_pars=freq_grid_pars):
        '''
        finds the fixed points and LFP spectra of the "ssn" object with modified parameters
        encoded in "params" for all stimulus inputs encoded in "stims".

        NOTE: seems like the grid_pars input argument is redundant/unused
        r_fps: if not None, it must have shape = (stims.shape[0], ssn.N), or really (b/c in this case stims is not used)
               it must just have shape = (anything>0, ssn.N)
        OUT:

        '''
        ssn_pars, noise_pars = self.ssn_pars, self.noise_pars

        ssn = SSN_classes.SSN_2D_AMPAGABA(ssn_pars.n, ssn_pars.k, ssn_pars.tauE, ssn_pars.tauI,
                                               *params[:4], tau_s=ssn_pars.tau_s, NMDAratio=params[6])
        ssn.NMDAratio = params[6]
        gE, gI = params[4:6]

        spect = []
        lams = []
        rs = []
        fs = None
        r_init = np.zeros(ssn.N)
        for con in contrasts:
            inp_vec = np.array([gE,gI]) * con + self.spont_input
            r_fp, CONVG = ssn.fixed_point_r(inp_vec, r_init=r_init, Tmax=Tmax, dt=dt, xtol=xtol)
            r_init = r_fp

            if not CONVG:
                break

            psE, fs, _, jac_eigs, _ = ssn.linear_power_spect(r_fp, noise_pars, **freq_grid_pars, EIGS=True)

            rs.append(r_fp)
            spect.append(psE)
            lams.append(jac_eigs)

        rs = np.array(rs)    # shape = (#contrasts, 2)
        spect = np.array(spect) # shape = (#contrasts, #freqs)
        lams = np.array(lams)   # shape = (#contrasts, 6)

        return rs, spect, fs, lams, ssn


    def sample_params(self):
        params = []
        for range in self.par_ranges:
            params.append(rand_samp(*range))

        if self.sampling_hyper['sigIElarger']:  # assure sigIE > sigEE
            sigEE = params[-2]
            sigIE_max = self.par_ranges[-1, -1]
            # assure further that  sigIE < 2*sigEE and sigXE_max:
            params[-1] = rand_samp(sigEE, np.minimum(sigIE_max, 2 * sigEE))
            # params.append(rand_samp(sigEE, sigXE_max) )

        return params


    def sample(self, Nsamps, freq_grid_pars=freq_grid_pars, params_2d=None, verbatim=False, remake_ssn=True):
        """
        :signature: sample(Nsamps, params_2d=None, freq_grid_pars=freq_grid_pars)
        :param Nsamps: number of samples
        :param params_2d: if provided, function will not sample the first 7 parameters (shared with 2-neuron model)
                          and will use the provided fixed parameters (just 7 numbers) for all samples.
        :return: arrays of sampled
                params, rs, spects, eigenvalues, j, fs, cond_inds, ssn
        """
        get_peak_2d = self.get_peak_2d # if get_peak_2d is None else get_peak_2d
        BALANCED = self.sampling_hyper['BALANCED'] # if  BALANCED is None else BALANCED
        check_dfdc_2d = self.sampling_hyper['check_dfdc_2d'] # if  check_dfdc_2d is None else check_dfdc_2d

        model = "full model" if self.ranges_dict['p_loc_max'] > 0 else "NL model"
        pbardesc = "% Samples, " + model

        smp = 0
        N_no_2d_cdep = 0
        params_list = []
        rs_list = []
        spect_list = []
        lams_list = []
        fs = None # just in case we don't get any samples
        pbar = tqdm(total=Nsamps, desc=pbardesc)
        for j in range(1000*Nsamps*(1 + 9*check_dfdc_2d)):
            params = self.sample_params()
            if params_2d is not None:
                params[:7] = params_2d

            Jee, Jei, Jie, Jii = params[:4]
            ge, gi = params[4:6]
            # guarantee positive det(J2x2):
            if not (Jee/Jie < Jei/Jii):
                continue
            # if BALANCED=True, guarantee positive Omega's
            if BALANCED and not (Jei/Jii < ge/gi):
                continue

            # if check_dfdc: check if the 2D model w same J's and g's yields global c-dependence; if not reject params.
            if check_dfdc_2d:
                contrasts = self.input_pars['contrasts']
                _, spect2d, fs2d, *_ = self.ssn_2D_PS(params[:7], contrasts[1:], freq_grid_pars=freq_grid_pars)
                # makes sure fixed_point converged for all contrasts:
                if len(spect2d) < len(contrasts[1:]) or fs2d is None:
                    continue

                f0, *_ = get_peak_2d(fs2d, spect2d.T) # Caleb had: SSN_power_spec.infl_find_peak_freq(fs2d, spect2d.T)
                dfdc2d = np.diff(f0) / np.diff(contrasts[1:])
                if np.any(np.isnan(dfdc2d)):
                    N_no_2d_cdep += 1
                    if verbatim:
                        print("Didn't satisfy 2D gamma test for df/dc.")
                    continue

            r_fps, spect, fs, lams, CONVG = self.ssn_retino_PS(params, freq_grid_pars=freq_grid_pars,
                                                               remake_ssn=remake_ssn)
            # check if all stims had converged to an f.p.
            if not CONVG:
                continue

            smp += 1
            pbar.update(1)
            pbar.set_postfix({"samp per j ": int(100 * (smp + 1)/ (j + 1)), "samp ": smp})

            params_list.append(params)
            rs_list.append(r_fps)
            spect_list.append(spect)
            lams_list.append(lams)

            if smp == Nsamps:
                break

        results = dict(params=np.asarray(params_list), r_fps=np.asarray(rs_list),
                       spects=np.asarray(spect_list), lams=np.asarray(lams_list), fs=fs, cond_inds=self.cond_inds)

        return results, j, N_no_2d_cdep, self.ssn


# ===========================================================================
# 2D retinotopic V1 model

class SamplerRetinoSSN():
    """
    sample retinotopic V1 SSN's and get their f.p.'s and spectra at a range of
    stimulus conditions specified by contrasts, radii, sigma_Gabor (according to make_RayMaunsell2010_inputs)
    """
    def __init__(self, sampling_ranges_dict, input_pars, fixed_conn_pars,
                 BALANCED=True, sigIElarger=False, check_dfdc_2d=True, get_peak_2d='infl_find_peak_freq',
                 ssn_pars=ssn_pars_2dV1, grid_pars=grid_pars_small, noise_pars=noise_pars):
        """
        sample retinotopic V1 SSN's and get their f.p.'s and spectra at a range of
        stimulus conditions specified by contrasts, radii, sigma_Gabor (according to make_RayMaunsell2010_inputs)

            self.input_pars = dict(contrasts=contrasts,
                                   radii=radii,
                                   n_probes=n_probes,
                                   sigma_Gabor=sigma_Gabor,
                                   sigma_RF=sigma_RF,
                                   sig_ori_EF=sig_ori_EF)

            self.fixed_conn_pars = dict(sigEI=sigEI,
                                        sigII=sigII,
                                        sigma_oris=sigma_oris,
                                        PERIODIC=PERIODIC)

        """
        self.param_names = ['Jee', 'Jei', 'Jie', 'Jii', 'ge', 'gi', 'ρN', 'λEE', 'λIE', 'σEE', 'σIE']
        if sampling_ranges_dict is not None: # secondary use of class is just to use retino_SSN_fixedparams
            self.make_par_ranges_array(sampling_ranges_dict)
        self.sampling_hyper = dict(BALANCED=BALANCED, sigIElarger=sigIElarger, check_dfdc_2d=check_dfdc_2d)
        self.n_probes = input_pars['n_probes']
        self.ssn_pars = ssn_pars
        self.grid_pars = grid_pars
        self.input_pars = input_pars
        self.fixed_conn_pars = fixed_conn_pars
        self.get_peak_2d = get_peak_dict[get_peak_2d]
        self.spont_input = ssn_pars.spont_input

        self.ssn = SSN_classes.SSN2DTopoV1_AMPAGABA(ssn_pars.n, ssn_pars.k, ssn_pars.tauE,
                                                    ssn_pars.tauI, self.grid_pars, conn_pars=None, tau_s=ssn_pars.tau_s)

        self.ssn.noise_pars = noise_pars # for historical reasons
        self.noise_pars = noise_pars

        out = self.make_RayMaunsell2010_inputs(ONLY_E=True, **self.input_pars)
        self.stims, self.cond_c_r, c_dep_inds, r_dep_inds, _, input_pars = out
        self.input_pars.update(input_pars)

        # probes go from 0 to 1 degrees of v.a. away from Gabor center along x-axis
        # last_prb = np.maximum(1, grid_pars_large.gridsize_deg/2) * ssn.grid_pars.magnif_factor
        # probe_xys = np.vstack((np.linspace(0,last_prb,n_probes), np.zeros(n_probes))).T
        self.probe_xys = np.vstack((np.arange(self.n_probes) * self.ssn.grid_pars.dx, np.zeros(self.n_probes))).T
        self.e_LFP = self.ssn.make_eLFP_from_xy(self.probe_xys, LFPradius=0.1, unit_xys="mm")

        gabor_ps_inds = self.stims.shape[0] - 1 + np.arange(self.e_LFP.shape[1])
        self.cond_inds = dict(c_dep=c_dep_inds, r_dep=r_dep_inds,
                              gabor_fp=self.stims.shape[0] - 1, gabor_ps=gabor_ps_inds)


    def make_par_ranges_array(self, ranges_dict):
        self.ranges_dict = ranges_dict
        keys = ranges_dict.keys()

        new_keys = ['J_E_min', 'J_I_min', 'sigEE_min', 'sigIE_min']
        old_keys = ['J_min',   'J_min',   'sigXE_min', 'sigXE_min']
        for new_min, old_min in zip(new_keys, old_keys):
            self.ranges_dict[new_min] = ranges_dict[new_min] if new_min in keys else ranges_dict[old_min]
            new_max, old_max = new_min.replace('min', 'max'), old_min.replace('min', 'max')
            self.ranges_dict[new_max] = ranges_dict[new_max] if new_max in keys else ranges_dict[old_max]
        del ranges_dict['J_min'], ranges_dict['J_max'], ranges_dict['sigXE_min'], ranges_dict['sigXE_max']

        range_keys = ['J_E', 'J_I', 'J_E', 'J_I', 'g', 'g', 'NMDA', 'p_loc', 'p_loc', 'sigEE', 'sigIE']
        self.par_ranges = np.zeros((len(self.param_names), 2))
        for i, key in enumerate(range_keys):
            self.par_ranges[i] = [self.ranges_dict[key + '_min'], self.ranges_dict[key + '_max']]


    def make_RayMaunsell2010_inputs(self, contrasts, radii, sigma_Gabor, sigma_RF, n_probes=None,
                                           sig_ori_EF=None, sig_ori_IF=None, spont_input=None, ONLY_E=False):
        """
        make full SSN input vectors for all the conditions of Ray & Maunsell as well
        as condition necessary for getting a size tuning curve.
        The conditions will be, in order, gratings of all "contrasts" at maximum
        radius (i.e. max(radii)), skipping the max(contrasts) & max(radii) grating (to avoid repetition),
        then gratings of all "radii" at max contrasts (i.e. max(contrasts)), and then
        the Ray & Maunsell 2010's Gabor stimulus.

        In:
            ssn: object of type SSN_classes.SSN2DTopoV1 or its descendents
            contrasts: vector of different contrasts (assumed to be in increasing order)
            radii: vector of different radii (assumed to be in increasing order)
            sigma_Gabor: (scalar) the sigma of the Gabor stimulus
            sigma_RF: the margin size (= V1 receptive field sigma) for grating stimulus inputs.
            ONLY_E: if True only output the E components of the stimuli
        Out:
            stims: shape = (#conditions, #neurons) condition (where #neuron = ssn.N)
                            and #conditions = len(contrasts) + len(radii)
            cond_c_r: shape = (#conditions, 2), cond_c_r[0] gives the (peak) contrast
                      in different conditions, while cond_c_r[0] gives the radius of sigma_Gabor
            c_dep_inds: indices of conditions relevant for contrast tuning/dependence
            r_dep_inds: indices of conditions relevant for size tuning/dependence
            gabor_inds: index of the Gabor stimulsu condition
        """
        assert isinstance(self.ssn, SSN_classes.SSN2DTopoV1)
        ssn = self.ssn

        conts = np.hstack((contrasts, np.ones(len(radii))*np.max(contrasts)))
        rads = np.hstack((np.max(radii)*np.ones(len(contrasts)-1), radii))
        cond_c_r = np.vstack((conts, np.hstack((rads, sigma_Gabor)))).T

        c_dep_inds = np.r_[0:len(contrasts)-1, len(contrasts)+len(radii)-2] # indices of contrast-dependence conditions
        r_dep_inds = np.r_[len(contrasts)-1:len(contrasts)+len(radii)-1] # indices of contrast-dependence conditions
        gabor_inds = np.array(len(cond_c_r)-1)

        input_pars = dict(contrasts=contrasts, radii=radii,
                          sigma_RF=sigma_RF, sigma_Gabor=sigma_Gabor)
        kwargs = {}
        if sig_ori_EF is not None:
            kwargs['sig_ori_EF'] = sig_ori_EF
            kwargs['sig_ori_IF'] = sig_ori_IF if sig_ori_IF is not None else sig_ori_EF
        input_pars.update(kwargs)


        stims = []
        for c, r in cond_c_r[:-1]:
            stims.append( c * ssn.make_grating_input(r, sigma_RF=sigma_RF, ONLY_E=ONLY_E, **kwargs) )
        stims.append( 100 * ssn.make_gabor_input(sigma_Gabor, ONLY_E=ONLY_E, **kwargs))

        return np.vstack(stims), cond_c_r, c_dep_inds, r_dep_inds, gabor_inds, input_pars


    # ssn_retino_FP finds the PS and fixed point
    def ssn_retino_PS(self, params, freq_grid_pars=freq_grid_pars, r_fps=None, spect_conds=None, remake_ssn=False,
                      verbatim=False, just_ssn=False):
        '''
        finds the fixed points and LFP spectra of the "ssn" object with modified parameters
        encoded in "params" for all stimulus inputs encoded in "stims".

        NOTE: seems like the grid_pars input argument is redundant/unused
        r_fps: if not None, it must have shape = (stims.shape[0], ssn.N), or really (b/c in this case stims is not used)
               it must just have shape = (anything>0, ssn.N)
        OUT:

        '''
        remake_ssn = True if just_ssn else remake_ssn
        if remake_ssn:
            self.ssn = SSN_classes.SSN2DTopoV1_AMPAGABA(self.ssn.n, self.ssn.k, self.ssn.neuron_params['tauE'],
                self.ssn.neuron_params['tauI'], self.grid_pars, conn_pars=None, ori_map=self.ssn.ori_map, tau_s=self.ssn.tau_s)
            self.ssn.noise_pars = self.noise_pars # for historical reasons

        ssn, stims, e_LFP = self.ssn, self.stims, self.e_LFP
        ssn_pars, grid_pars, noise_pars = self.ssn_pars, self.grid_pars, self.noise_pars

        #unpack parameters
        gE, gI, NMDAratio, p_local_EE = params[4:8]
        p_local_IE = params[8] if (len(params) == 11) else p_local_EE
        sigEE, sigIE = params[9:11] if (len(params) == 11) else params[8:10]

        conn_pars = dict(
            J_2x2 = ssn_pars.make_J2x2(*params[:4]),
            s_2x2 = np.array([[sigEE, self.fixed_conn_pars['sigEI']],
                              [sigIE, self.fixed_conn_pars['sigII']]]), # in mm
            p_local = [p_local_EE, p_local_IE],
            sigma_oris=self.fixed_conn_pars['sigma_oris'],
            PERIODIC=self.fixed_conn_pars['PERIODIC'])

        ssn.make_W(**conn_pars)
        ssn.NMDAratio = NMDAratio

        if just_ssn: # just return the ssn with made W with given parameters
            return ssn
        else: # calculate fixed point r's, PS's, and Jacobian eigenvalues
            if r_fps is None:
                inps = np.hstack((gE * stims, gI * stims)).T # shape: (ssn.N, #conditions)
                # for inp_vec in inps.T:
                #     r_init = np.zeros(ssn.N)
                #     r_fp, CONVG = ssn.fixed_point_r(inp_vec, r_init=r_init, Tmax=Tmax, dt=dt, xtol=xtol)
                r_init = np.zeros(inps.shape)
                r_fps, CONVG = ssn.fixed_point_r(inps, r_init=r_init, Tmax=Tmax, dt=dt, xtol=xtol)
                r_fps = r_fps.T # shape = (#conditions, ssn.N) = (stims.shape[0], ssn.N)
            else:
                assert r_fps.shape[0] > 0 and r_fps.shape[1] == ssn.N

            spect = []
            lams = []
            fs = None
            if CONVG:
                if verbatim: # for debugging
                    attr_dir1 = lambda A, sep=" | ": print(sep.join(sorted([f"{d}: {(A.__dict__)[d]}" for d in dir(A) if not callable(getattr(A, d)) and d[:1] != '_'])))
                    print("spect_conds = ", spect_conds, "\ne_LFP nonzero: ", e_LFP.nonzero()[0], "\nfreq_grid_pars", freq_grid_pars, "\nnoise_pars", attr_dir1(noise_pars))

                # first get LFP-PS at center probe in all (if spect_conds is None) grating conditions:
                r_fps_spect = r_fps[:-1] if spect_conds is None else r_fps[spect_conds]
                for r_fp in r_fps_spect:
                    psE, _, _, jac_eigs, _ = ssn.linear_power_spect(r_fp, noise_pars,
                                             **freq_grid_pars, e_LFP=e_LFP[:,0], EIGS=True)
                    spect.append(psE)
                    lams.append(jac_eigs)
                spect = np.asarray(spect)

                # then get LFP-PS at all probes in Gabor condition
                psE, fs, _, jac_eigs, _ = ssn.linear_power_spect(r_fps[-1], noise_pars,
                                                  **freq_grid_pars, e_LFP=e_LFP, EIGS=True)
                spect = np.vstack((spect, psE)) # shape: (stims.shape[0] + e_LFP.shape[1]-1, # freqs) if spect_conds is None
                lams.append(jac_eigs)
                lams = np.asarray(lams) # shape = (stims.shape[0], ssn.dim) if spect_conds is None

                if verbatim: # for debugging
                    print(".ssn_retino_PS: SD(spect, axis=1)[2:5]", np.std(spect, axis=1)[2:5])
                    print(".ssn_retino_PS: SD(r_fps, axis=1)[2:5]", np.std(r_fps, axis=1)[2:5])
                    print(".ssn_retino_PS: params = ", params)

            return r_fps, spect, fs, lams, CONVG


    def ssn_2D_PS(self, params, contrasts, spect_conds=None, r_fps=None, freq_grid_pars=freq_grid_pars):
        '''
        finds the fixed points and LFP spectra of the "ssn" object with modified parameters
        encoded in "params" for all stimulus inputs encoded in "stims".

        NOTE: seems like the grid_pars input argument is redundant/unused
        r_fps: if not None, it must have shape = (stims.shape[0], ssn.N), or really (b/c in this case stims is not used)
               it must just have shape = (anything>0, ssn.N)
        OUT:

        '''
        ssn_pars, noise_pars = self.ssn_pars, self.noise_pars

        ssn = SSN_classes.SSN_2D_AMPAGABA(ssn_pars.n, ssn_pars.k, ssn_pars.tauE, ssn_pars.tauI,
                                               *params[:4], tau_s=ssn_pars.tau_s, NMDAratio=params[6])
        ssn.NMDAratio = params[6]
        gE, gI = params[4:6]

        spect = []
        lams = []
        rs = []
        fs = None
        r_init = np.zeros(ssn.N)
        for con in contrasts:
            inp_vec = np.array([gE,gI]) * con + self.spont_input
            r_fp, CONVG = ssn.fixed_point_r(inp_vec, r_init=r_init, Tmax=Tmax, dt=dt, xtol=xtol)
            r_init = r_fp
            
            if not CONVG:
                break
                
            psE, fs, _, jac_eigs, _ = ssn.linear_power_spect(r_fp, noise_pars, **freq_grid_pars, EIGS=True)

            rs.append(r_fp)
            spect.append(psE)
            lams.append(jac_eigs)

        rs = np.array(rs)    # shape = (#contrasts, 2)
        spect = np.array(spect) # shape = (#contrasts, #freqs)
        lams = np.array(lams)   # shape = (#contrasts, 6)

        return rs, spect, fs, lams, ssn


    def sample_params(self):
        params = []
        for range in self.par_ranges:
            params.append(rand_samp(*range))

        if self.sampling_hyper['sigIElarger']:  # assure sigIE > sigEE
            sigEE = params[-2]
            sigIE_max = self.par_ranges[-1, -1]
            # assure further that  sigIE < 2*sigEE and sigXE_max:
            params[-1] = rand_samp(sigEE, np.minimum(sigIE_max, 2 * sigEE))
            # params.append(rand_samp(sigEE, sigXE_max) )

        return params


    def sample(self, Nsamps, freq_grid_pars=freq_grid_pars, params_2d=None, verbatim=False, remake_ssn=True):
        """
        :signature: sample(Nsamps, params_2d=None, freq_grid_pars=freq_grid_pars)
        :param Nsamps: number of samples
        :param params_2d: if provided, function will not sample the first 7 parameters (shared with 2-neuron model)
                          and will use the provided fixed parameters (just 7 numbers) for all samples.
        :return: arrays of sampled
                params, rs, spects, eigenvalues, j, fs, cond_inds, ssn
        """
        get_peak_2d = self.get_peak_2d # if get_peak_2d is None else get_peak_2d
        BALANCED = self.sampling_hyper['BALANCED'] # if  BALANCED is None else BALANCED
        check_dfdc_2d = self.sampling_hyper['check_dfdc_2d'] # if  check_dfdc_2d is None else check_dfdc_2d

        model = "full model" if self.ranges_dict['p_loc_max'] > 0 else "NL model"
        pbardesc = "% Samples, " + model

        smp = 0
        N_no_2d_cdep = 0
        params_list = []
        rs_list = []
        spect_list = []
        lams_list = []
        fs = None # just in case we don't get any samples
        pbar = tqdm(total=Nsamps, desc=pbardesc)
        for j in range(1000*Nsamps*(1 + 9*check_dfdc_2d)):
            params = self.sample_params()
            if params_2d is not None:
                params[:7] = params_2d

            Jee, Jei, Jie, Jii = params[:4]
            ge, gi = params[4:6]
            # guarantee positive det(J2x2):
            if not (Jee/Jie < Jei/Jii):
                continue
            # if BALANCED=True, guarantee positive Omega's
            if BALANCED and not (Jei/Jii < ge/gi):
                continue

            # if check_dfdc: check if the 2D model w same J's and g's yields global c-dependence; if not reject params.
            if check_dfdc_2d:
                contrasts = self.input_pars['contrasts']
                _, spect2d, fs2d, *_ = self.ssn_2D_PS(params[:7], contrasts[1:], freq_grid_pars=freq_grid_pars)
                # makes sure fixed_point converged for all contrasts:
                if len(spect2d) < len(contrasts[1:]) or fs2d is None: 
                    continue
                    
                f0, *_ = get_peak_2d(fs2d, spect2d.T) # Caleb had: SSN_power_spec.infl_find_peak_freq(fs2d, spect2d.T)
                dfdc2d = np.diff(f0) / np.diff(contrasts[1:])
                if np.any(np.isnan(dfdc2d)):
                    N_no_2d_cdep += 1
                    if verbatim:
                        print("Didn't satisfy 2D gamma test for df/dc.")
                    continue

            r_fps, spect, fs, lams, CONVG = self.ssn_retino_PS(params, freq_grid_pars=freq_grid_pars,
                                                               remake_ssn=remake_ssn)
            # check if all stims had converged to an f.p.
            if not CONVG:
                continue

            smp += 1
            pbar.update(1)
            pbar.set_postfix({"samp per j ": int(100 * (smp + 1)/ (j + 1)), "samp ": smp})

            params_list.append(params)
            rs_list.append(r_fps)
            spect_list.append(spect)
            lams_list.append(lams)

            if smp == Nsamps:
                break

        results = dict(params=np.asarray(params_list), r_fps=np.asarray(rs_list),
                       spects=np.asarray(spect_list), lams=np.asarray(lams_list), fs=fs, cond_inds=self.cond_inds)

        return results, j, N_no_2d_cdep, self.ssn


    def ssn_retino_PS_yesno_radii(self, params, spect_radii=True, freq_grid_pars=freq_grid_pars, r_fps=None):
        """
        Wrapper function for SamplerRetinoSSN.ssn_retino_PS which allows you to tell it not to calculate PS
        for different radii condition -- made for backward compatibility.
        """
        cond_inds = self.cond_inds.copy()
        cond_inds['r_dep_ps'] = cond_inds['r_dep'] if spect_radii else None
        cond_inds['c_dep_ps'] = cond_inds['c_dep'] if spect_radii else np.arange(len(cond_inds['c_dep']))
        if not spect_radii:
            gabor_ps_start = len(cond_inds['c_dep'])
            cond_inds['gabor_ps'] = gabor_ps_start + np.arange(self.e_LFP.shape[1])

        r_fps, spect, fs, lams, CONVG = self.ssn_retino_PS(params, freq_grid_pars=freq_grid_pars, r_fps=r_fps,
                                                           spect_conds=None if spect_radii else cond_inds['c_dep'])

        return r_fps, spect, lams, CONVG, fs, cond_inds, self.ssn


# --------------------------------------------------------------------------------------------------------------


def retino_SSN_fixedparams(params, input_pars, fixed_conn_pars,
                           ssn_pars=ssn_pars_2dV1, grid_pars=grid_pars_small,
                           noise_pars=noise_pars, freq_grid_pars=freq_grid_pars,
                           spect_radii=True, r_fps=None, just_ssn=False):
    """
    Wrapper function for SamplerRetinoSSN.ssn_retino_PS_yesno_radii to use it for a given parameter set (params)
    without creating a sampler object.
    """
    sampler = SamplerRetinoSSN(sampling_ranges_dict=None,
                   input_pars=input_pars, fixed_conn_pars=fixed_conn_pars,
                   ssn_pars=ssn_pars, grid_pars=grid_pars, noise_pars=noise_pars)

    if just_ssn:
        ssn = sampler.ssn_retino_PS(params, just_ssn=True)
        return ssn
    else:
        if spect_radii:
            r_fps, spect, fs, lams, CONVG = sampler.ssn_retino_PS(params, freq_grid_pars=freq_grid_pars, r_fps=r_fps)
            return r_fps, spect, lams, CONVG, fs, sampler.cond_inds, sampler.ssn
        else:
            return sampler.ssn_retino_PS_yesno_radii(params, spect_radii=spect_radii,
                                                             freq_grid_pars=freq_grid_pars, r_fps=r_fps)





# # Utility functions kept for historical reasons (backward compatibility just in case)
# def retino_SSN_fixedparams_old(contrasts, radii, params_2d, p_loc_EE, p_loc_IE, sigEE, sigIE,
#                            sigEI=.09, sigII=.09, PERIODIC=True, sigma_oris=None,
#                            n_probes=5, sigma_Gabor=0.5, sigma_RF=0.4, sig_ori_EF=None,
#                            ssn_pars=ssn_pars_2dV1, grid_pars=grid_pars_small, noise_pars=noise_pars,
#                            spect_radii=True, r_fps=None):
#     """
#     Wrapper function for SamplerRetinoSSN.ssn_retino_PS_yesno_radii to use it for a given parameter set (params)
#     without creating a sampler object.
#     """
#     input_pars = dict(contrasts=contrasts,
#                   radii=radii,
#                   n_probes=n_probes,
#                   sigma_Gabor=sigma_Gabor,
#                   sigma_RF=sigma_RF,
#                   sig_ori_EF=sig_ori_EF)
#
#     fixed_conn_pars = dict(sigEI=sigEI,
#                        sigII=sigII,
#                        sigma_oris=sigma_oris,
#                        PERIODIC=PERIODIC)
#
#     sampler = SamplerRetinoSSN(sampling_ranges_dict=None,
#                    input_pars=input_pars, fixed_conn_pars=fixed_conn_pars,
#                    ssn_pars=ssn_pars, grid_pars=grid_pars, noise_pars=noise_pars)
#
#     params = params_2d + [p_loc_EE, p_loc_IE, sigEE, sigIE]
#
#     return sampler.ssn_retino_PS_yesno_radii(params, spect_radii=spect_radii, r_fps=r_fps)
#
#
