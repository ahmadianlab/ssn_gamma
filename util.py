import numpy as np
#import jax.numpy as np

def Euler2fixedpt(dxdt, x_initial, Tmax, dt, xtol=1e-5, xmin=1e-0, Tmin=200, PLOT=False, inds=None, verbose=False, silent=False, Tfrac_CV=0):
    """
    Finds the fixed point of the D-dim ODE set dx/dt = dxdt(x), using the
    Euler update with sufficiently large dt (to gain in computational time).
    Checks for convergence to stop the updates early.

    IN:
    dxdt = a function handle giving the right hand side function of dynamical system
    x_initial = initial condition for state variables (a column vector)
    Tmax = maximum time to which it would run the Euler (same units as dt, e.g. ms)
    dt = time step of Euler
    xtol = tolerance in relative change in x for determining convergence
    xmin = for x(i)<xmin, it checks convergenece based on absolute change, which must be smaller than xtol*xmin
        Note that one can effectively make the convergence-check purely based on absolute,
        as opposed to relative, change in x, by setting xmin to some very large
        value and inputting a value for 'xtol' equal to xtol_desired/xmin.
    PLOT: if True, plot the convergence of some component
    inds: indices of x (state-vector) to plot
    verbose: if True print convergence criteria even if passed (function always prints out a warning if it doesn't converge).
    Tfrac_var: if not zero, maximal temporal CV (coeff. of variation) of state vector components, over the final
               Tfrac_CV fraction of Euler timesteps, is calculated and printed out.
               
    OUT:
    xvec = found fixed point solution
    CONVG = True if determined converged, False if not
    """

    if PLOT:
        if inds is None:
            x_dim = x_initial.size
            inds = [int(x_dim/4), int(3*x_dim/4)]
        xplot = x_initial.flatten()[inds][:,None]

    Nmax = int(np.round(Tmax/dt))
    Nmin = int(np.round(Tmin/dt)) if Tmax > Tmin else int(Nmax/2)
    xvec = x_initial
    CONVG = False

    if Tfrac_CV > 0:
        xmean = np.zeros_like(xvec)
        xsqmean = np.zeros_like(xvec)
        Nsamp = 0

    for n in range(Nmax):
        dx = dxdt(xvec) * dt
        xvec = xvec + dx
        if PLOT:
            xplot = np.hstack((xplot, xvec.flatten()[inds][:,None]))
        
        if Tfrac_CV > 0 and n >= (1-Tfrac_CV) * Nmax:
            xmean = xmean + xvec
            xsqmean = xsqmean + xvec**2
            Nsamp = Nsamp + 1

        if n > Nmin:
            if np.abs( dx /np.maximum(xmin, np.abs(xvec)) ).max() < xtol:
                if verbose:
                    print("      converged to fixed point at iter={},      as max(abs(dx./max(xvec,{}))) < {} ".format(n, xmin, xtol))
                CONVG = True
                break

    if not CONVG and not silent: # n == Nmax:
        print("\n Warning 1: reached Tmax={}, before convergence to fixed point.".format(Tmax))
        print("       max(abs(dx./max(abs(xvec), {}))) = {},   xtol={}.\n".format(xmin, np.abs( dx /np.maximum(xmin, np.abs(xvec)) ).max(), xtol))

        if Tfrac_CV > 0:
            xmean = xmean/Nsamp
            xvec_SD = np.sqrt(xsqmean/Nsamp - xmean**2)
            # CV = xvec_SD / xmean
            # CVmax = CV.max()
            CVmax = xvec_SD.max() / xmean.max()
            print(f"max(SD)/max(mean) of state vector in the final {Tfrac_CV:.2} fraction of Euler steps was {CVmax:.5}")

        #mybeep(.2,350)
        #beep

    if PLOT:
        import matplotlib.pyplot as plt
        plt.figure(244459)
        plt.plot(np.arange(n+2)*dt, xplot.T, 'o-')

    return xvec, CONVG



# this is copied from scipy.linalg, to make compatible with jax.numpy
def toeplitz(c, r=None):
    """
    Construct a Toeplitz matrix.
    The Toeplitz matrix has constant diagonals, with c as its first column
    and r as its first row.  If r is not given, ``r == conjugate(c)`` is
    assumed.
    Parameters
    ----------
    c : array_like
        First column of the matrix.  Whatever the actual shape of `c`, it
        will be converted to a 1-D array.
    r : array_like
        First row of the matrix. If None, ``r = conjugate(c)`` is assumed;
        in this case, if c[0] is real, the result is a Hermitian matrix.
        r[0] is ignored; the first row of the returned matrix is
        ``[c[0], r[1:]]``.  Whatever the actual shape of `r`, it will be
        converted to a 1-D array.
    Returns
    -------
    A : (len(c), len(r)) ndarray
        The Toeplitz matrix. Dtype is the same as ``(c[0] + r[0]).dtype``.
    See also
    --------
    circulant : circulant matrix
    hankel : Hankel matrix
    Notes
    -----
    The behavior when `c` or `r` is a scalar, or when `c` is complex and
    `r` is None, was changed in version 0.8.0.  The behavior in previous
    versions was undocumented and is no longer supported.
    Examples
    --------
    >>> from scipy.linalg import toeplitz
    >>> toeplitz([1,2,3], [1,4,5,6])
    array([[1, 4, 5, 6],
           [2, 1, 4, 5],
           [3, 2, 1, 4]])
    >>> toeplitz([1.0, 2+3j, 4-1j])
    array([[ 1.+0.j,  2.-3.j,  4.+1.j],
           [ 2.+3.j,  1.+0.j,  2.-3.j],
           [ 4.-1.j,  2.+3.j,  1.+0.j]])
    """
    c = np.asarray(c).ravel()
    if r is None:
        r = c.conjugate()
    else:
        r = np.asarray(r).ravel()
    # Form a 1D array of values to be used in the matrix, containing a reversed
    # copy of r[1:], followed by c.
    vals = np.concatenate((r[-1:0:-1], c))
    a, b = np.ogrid[0:len(c), len(r) - 1:-1:-1]
    indx = a + b
    # `indx` is a 2D array of indices into the 1D array `vals`, arranged so
    # that `vals[indx]` is the Toeplitz matrix.
    return vals[indx]


def lin_interp1(ts, ys, t, t0, dt):
    if t >= ts[-1]:
        y = ys[:, -1]
    else:
        iL = int(np.floor((t-t0) / dt)) #since t is smaller than ts[-1] and size(ys,2) == Nt, with Nt = (ts(end)-t0)/dt, then iL < Nt, so iL <=size(ys,2)
        pR = (t - ts[iL]) / dt
        y = (1 - pR) * ys[:, iL] + pR * ys[:, iL+1]

    return y


def welch_spectrum(x_ts, df, dt=1, Tmax=50000, Nt_min=0, Kmin=4, window_type='welch'):
    """
    Calculate (an estimate of) the power spectral density of the signals x_ts using
    the Welch method.
    SSN usage: as input to this functino you want to give (in the 2 neuron model)
               np.sum(v_dyn[:, ::2, :], axis=1)
    which would add currents into E cells through all receptors. For multi-neuron case replace ::2 with ::2*ssn.Ne
    :param x_ts: a 2D array of size (N_conds, Nt) where for each c, x_ts[c] is a time-series
    :param df: frequency resolution (Hz) -- the length/duration of segments or windows (in number of time-steps)
               is based on this via Nt_seg = number-of-time-steps-per-segment = 1 / (df * dt)    (up to rounding)
    :param dt: time-step of signal(s) in seconds
    :param Tmax: Total duration of signal(s) to consider, in seconds
    :param Nt_min: drop the first Nt_min - 1 time-steps
    :param Kmin: Use at least this many disjoint segments
    :param window_type: 'welch', 'bartlett', 'hamming', or 'hahn'
           Numerical Recipes recommends either Bartlett or Welch
    :return: psd the power spectral density. The sum of psd times df gives the total power which is the time average of
             the square of the (mean-centered) signal. More precisely, the time average is the combination of average
             over segments, and weighted time average within segments with weight given by window^2
    """
    x_ts = np.atleast_2d(x_ts)
    x_ts = x_ts[..., Nt_min:int(np.floor(Tmax / dt))]
    x_ts = x_ts - np.mean(x_ts, axis=-1, keepdims=True)
    Nt = x_ts.shape[-1] #number of time steps in the whole sequence, so the

    Nt_seg = int(np.round(1 / (df * dt))) # number of time steps in each segment, since df = 1/T_segment, then Nt_seg = T_segment/dt = (1/(df * dt))
    df = 1 / (Nt_seg * dt) # recalculate frequency resolution. This is K * df0 where df0 = 1/(Nt * dt) is the full freq-resolution of the full segment (if we hadn't chunked it.)

    K = int(np.floor(Nt / Nt_seg)) #number of chunks or segments = df * T = df * dt * Nt ( = bandwidth-to-fundamental frequency ratio)
    if K < Kmin:
        # recalculate Nt_seg and Nt
        K = Kmin
        Nt_seg = int(np.floor(Nt / K))
        df = 1 / (Nt_seg * dt)
        print(f"Warning: frequency resolution lowered, with df set to {df:.0f}, to allow for at least 4 disjoint segments in x_ts")
    dNt = Nt - K * Nt_seg # left-over time-steps

    # suppose Nt_seg = 2 * m + 1 is odd, so that m = floor(Nt_seg / 2)
    m = int(np.floor(Nt_seg / 2))
    if window_type == 'welch':
        window = 1 - ((np.arange(Nt_seg) - m) / m)**2
    elif window_type == 'bartlett':
        window = 1 - np.abs(np.arange(Nt_seg) - m) / m
    elif window_type == 'hamming':
        window = 0.54 - 0.46 * np.cos(2 * np.pi * np.arange(Nt_seg) / (Nt_seg-1))
    elif window_type == 'hann':
        window = 0.5 * (1 - np.cos(2 * np.pi * np.arange(Nt_seg) / (Nt_seg-1)))
    else:
        window = np.ones(Nt_seg)
    window_weight = np.sum(window**2) #without window_type this is Nt_seg

    psd = []
    for x_t in x_ts.reshape((-1, Nt)):

        # chunk and fold x_t into a 2D array of Nt_seg by (about) 2 * K
        x1 = np.reshape(x_t[:K * Nt_seg], (Nt_seg, K), 'F')
        half_overlap = np.reshape(x_t[m:(m + Nt_seg * (K-1))], (Nt_seg, K - 1), 'F')
        x1 = np.hstack((x1, half_overlap))
        if dNt > m / 2:
            x1 = np.hstack((x1, np.reshape(x_t[Nt-Nt_seg:Nt], (Nt_seg, 1))))
        x1 = window[:, None] * x1

        xf = np.fft.fft(x1, axis=0)
        # np.fft.fft returns xf[f] = \sum_t x[t] * exp(-2πi * f * t / Nt_seg)
        # to get a unitary transform you need to divide by sqrt(Nt_seg), hence the division by Nt_seg in following line.
        # with this normalization, sum(pf * df * window_weight) = sum(x1**2) or sum(pf * df) = sum(x1^2) / window_weight
        # so we see that the total integral of our psd is the average x(t)^2, where the temporal average is weighted
        # average with weights given by window**2 (this is easiest to see in the case of the naive window ones(Nt_seg)
        pf = np.mean(np.abs(xf)**2, axis=1) / Nt_seg / window_weight / df

        # average over positive and negative frequencies
        power = np.zeros(m + 1)
        power[0] = pf[0]

        if np.mod(Nt_seg, 2)==1: #if Nt_seg is odd
            power[1:] = pf[1:(m + 1)] + pf[Nt_seg:m:-1]
        else:
            power[1:m] = pf[1:m] + pf[Nt_seg:m:-1]
            power[m] = pf[m]

        psd.append(power)
    fs = np.arange(m + 1) * df
    psd = np.array(psd).reshape(x_ts.shape[:-1] + (len(psd[0]),))

    return psd, fs, Nt_seg
